/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Diese Klasse stellt eine Veranstaltung dar.
 * Enthält auch die Spielerliste und die Liste von Turnieren
 *
 * @author admin
 */
public class Event implements Serializable {
    private String name;
    private String date;
    private String time;
    private int minStartNumber;
    private ArrayList<Tournament> tournaments = new ArrayList<Tournament>();
    private ArrayList<Player> players = new ArrayList<Player>();
    
    //Konstruktor
    public Event(String name, String date, String time, int minStartNumber) {
        this.name = name;
        this.date = date;
        this.time = time;
        this.minStartNumber = minStartNumber;
    }
    
    // Fügt Spieler der Veranstaltung hinzu
    public void addPlayer(String firstName, String lastName, String nickName, String present, String paid, String hasSuperFree){
        Player player = new Player(firstName, lastName, nickName, present, paid, hasSuperFree);
        players.add(player);
    }
    
    // Entfernt Spieler
    public void removePlayer(){
        //TODO
    }
    
    // Gibt kleinste erlaubte Startnummer zurück
    public int getMinStartNumber(){
        return minStartNumber;
    }
    // Setzt kleinste erlaubte Startnummer
    public void setMinStartNumber(int minStartNumber){
        this.minStartNumber = minStartNumber;
    }
    
    // Erstellt neues Turnier
    public void addTournament(String name, String date, String time, int rounds, int playersInKO, int freePassValue, boolean doubleKO, boolean modified, boolean winPoints){
        Tournament tournament = new Tournament(name,date,time,rounds,playersInKO, doubleKO);
        tournament.setFreePassValue(freePassValue);
        tournament.setModified(modified);
        tournament.setUsesWinPoints(winPoints);
        tournaments.add(tournament);
    }
    
    public void removeTournament(){
        //TODO
    }
    
    
    public ArrayList<Tournament> getTournamentList(){
        return tournaments;
    }
    
    public ArrayList<Player> getPlayerList(){
        return players;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
}
