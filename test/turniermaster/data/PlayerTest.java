/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.data;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class PlayerTest {
    private Player testPlayer;
    
    public PlayerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
         testPlayer = new Player ("Theodor","Testheim","Testnick","Nein","Ja","Nein");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFirstName method, of class Player.
     */
    @Test
    public void testGetFirstName() {
        System.out.println("getFirstName");
        String expResult = "Theodor";
        String result = testPlayer.getFirstName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setFirstName method, of class Player.
     */
    @Test
    public void testSetFirstName() {
        System.out.println("setFirstName");
        String firstName = "Manfred";
        testPlayer.setFirstName(firstName);
        assertEquals(testPlayer.getFirstName(),firstName);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getLastName method, of class Player.
     */
    @Test
    public void testGetLastName() {
        System.out.println("getLastName");
        String expResult = "Testheim";
        String result = testPlayer.getLastName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setLastName method, of class Player.
     */
    @Test
    public void testSetLastName() {
        System.out.println("setLastName");
        String lastName = "Meier";
        testPlayer.setLastName(lastName);
        assertEquals(testPlayer.getLastName(),lastName);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getNickName method, of class Player.
     */
    @Test
    public void testGetNickName() {
        System.out.println("getNickName");
        String expResult = "Testnick";
        String result = testPlayer.getNickName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setNickName method, of class Player.
     */
    @Test
    public void testSetNickName() {
        System.out.println("setNickName");
        String nickName = "NeuNick";
        testPlayer.setNickName(nickName);
        assertEquals(testPlayer.getNickName(),nickName);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getStartNumber method, of class Player.
     */
    @Test
    public void testGetStartNumber() {
        System.out.println("getStartNumber");
        int expResult = 0;
        int result = testPlayer.getStartNumber();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setStartNumber method, of class Player.
     */
    @Test
    public void testSetStartNumber() {
        System.out.println("setStartNumber");
        int startNumber = 42;
        testPlayer.setStartNumber(startNumber);
        assertEquals(testPlayer.getStartNumber(),startNumber);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isPaid method, of class Player.
     */
    @Test
    public void testIsPaid() {
        System.out.println("isPaid");
        String expResult = "Ja";
        String result = testPlayer.isPaid();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setPaid method, of class Player.
     */
    @Test
    public void testSetPaid() {
        System.out.println("setPaid");
        String paid = "Nein";
        testPlayer.setPaid(paid);
        assertEquals(testPlayer.isPaid(),paid);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isPresent method, of class Player.
     */
    @Test
    public void testIsPresent() {
        System.out.println("isPresent");
        String expResult = "Nein";
        String result = testPlayer.isPresent();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setPresent method, of class Player.
     */
    @Test
    public void testSetPresent() {
        System.out.println("setPresent");
        String present = "Nein";
        testPlayer.setPresent(present);
        assertEquals(testPlayer.isPresent(),present);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
}
