/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.data;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class EventTest {
    
    private Event event = new Event ("Veranstaltung","2015-01-06","12:00",1);
    private Player player = new Player ("Theodor","Testheim","Testnick","Nein","Ja","Nein");
    
    public EventTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        event.addPlayer("Theodor","Testheim","Testnick","Nein","Ja","Nein");
        event.addTournament("Turnier","2015-01-06","12:00",6,16,3,false,false,false);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addPlayer method, of class Event.
     */
    @Test
    public void testAddPlayer() {
        System.out.println("addPlayer");
        String firstName = "Werner";
        String lastName = "von Testheim";
        String nickName = "Sir Test";
        event.addPlayer(firstName, lastName, nickName, "Nein", "Ja","Nein");
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(event.getPlayerList().get(event.getPlayerList().size()-1).getFirstName().equals("Werner"));
    }

    /**
     * Test of removePlayer method, of class Event.
     */
    @Test
    public void testRemovePlayer() {
        System.out.println("removePlayer");
        event.removePlayer();
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(true);
    }

    /**
     * Test of addTournament method, of class Event.
     */
    @Test
    public void testAddTournament() {
        System.out.println("addTournament");
        String name = "TestTurnier";
        String date = "2015-01-06";
        String time = "12:00";
        int rounds = 5;
        event.addTournament(name, date, time, rounds,16,3,false,false,false);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(event.getTournamentList().get(1).getName().equals("TestTurnier"));
    }

    /**
     * Test of removeTournament method, of class Event.
     */
    @Test
    public void testRemoveTournament() {
        System.out.println("removeTournament");
        event.removeTournament();
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(true);
    }

    /**
     * Test of getTournamentList method, of class Event.
     */
    @Test
    public void testGetTournamentList() {
        System.out.println("getTournamentList");
        ArrayList<Tournament> result = event.getTournamentList();
        assertTrue(result.size()==1);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getPlayerList method, of class Event.
     */
    @Test
    public void testGetPlayerList() {
        System.out.println("getPlayerList");
        ArrayList<Player> result = event.getPlayerList();
        assertTrue(result.size()==1);
    }

    /**
     * Test of getName method, of class Event.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expResult = "Veranstaltung";
        String result = event.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class Event.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "NeuerName";
        event.setName(name);
        assertEquals(event.getName(),name);
    }

    /**
     * Test of getDate method, of class Event.
     */
    @Test
    public void testGetDate() {
        System.out.println("getDate");
        String expResult = "2015-01-06";
        String result = event.getDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDate method, of class Event.
     */
    @Test
    public void testSetDate() {
        System.out.println("setDate");
        String date = "2014-05-07";
        event.setDate(date);
        assertEquals(event.getDate(),date);
    }

    /**
     * Test of getTime method, of class Event.
     */
    @Test
    public void testGetTime() {
        System.out.println("getTime");
        String expResult = "12:00";
        String result = event.getTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTime method, of class Event.
     */
    @Test
    public void testSetTime() {
        System.out.println("setTime");
        String time = "9:00";
        event.setTime(time);
        assertEquals(event.getTime(),time);
    }
    

    
}
