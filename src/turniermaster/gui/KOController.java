/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.gui;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import turniermaster.data.KOTree;
import turniermaster.data.Player;
import turniermaster.data.Tournament;

/**
 * FXML Controller class
 *
 * @author admin
 */
public class KOController implements Initializable {

    @FXML
    Button button11;
    @FXML
    Button button12;
    @FXML
    Button button21;
    @FXML
    Button button22;
    @FXML
    Button button31;
    @FXML
    Button button32;
    @FXML
    Button button41;
    @FXML
    Button button42;
    @FXML
    Button button51;
    @FXML
    Button button52;
    @FXML
    Button button61;
    @FXML
    Button button62;
    @FXML
    Button button71;
    @FXML
    Button button72;
    @FXML
    Button button81;
    @FXML
    Button button82;
    @FXML
    Button button91;
    @FXML
    Button button92;
    @FXML
    Button button101;
    @FXML
    Button button102;
    @FXML
    Button button111;
    @FXML
    Button button112;
    @FXML
    Button button121;
    @FXML
    Button button122;
    @FXML
    Button button131;
    @FXML
    Button button132;
    @FXML
    Button button141;
    @FXML
    Button button142;
    @FXML
    Button button151;
    @FXML
    Button button152;
    @FXML
    Button button161;
    @FXML
    Button button162;
    @FXML
    Button button171;
    @FXML
    Button button172;
    @FXML
    Button button181;
    @FXML
    Button button182;
    @FXML
    Button button191;
    @FXML
    Button button192;
    @FXML
    Button button201;
    @FXML
    Button button202;
    @FXML
    Button button211;
    @FXML
    Button button212;
    @FXML
    Button button221;
    @FXML
    Button button222;
    @FXML
    Button button231;
    @FXML
    Button button232;
    @FXML
    Button button241;
    @FXML
    Button button242;
    @FXML
    Button button251;
    @FXML
    Button button252;
    @FXML
    Button button261;
    @FXML
    Button button262;
    @FXML
    Button button271;
    @FXML
    Button button272;
    @FXML
    Button button281;
    @FXML
    Button button282;
    @FXML
    Button button291;
    @FXML
    Button button292;
    @FXML
    Button button301;
    @FXML
    Button button302;
    @FXML
    private Label eightFinalLabel;
    @FXML
    private Label quarterFinalLabel;

    private KOTree tree;
    private Tournament tournament;
    private List<Button> buttonList;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
        buttonList = new ArrayList<Button>();
        buttonList.add(button11);
        buttonList.add(button12);
        buttonList.add(button21);
        buttonList.add(button22);
        buttonList.add(button31);
        buttonList.add(button32);
        buttonList.add(button41);
        buttonList.add(button42);
        buttonList.add(button51);
        buttonList.add(button52);
        buttonList.add(button61);
        buttonList.add(button62);
        buttonList.add(button71);
        buttonList.add(button72);
        buttonList.add(button81);
        buttonList.add(button82);
        buttonList.add(button91);
        buttonList.add(button92);
        buttonList.add(button101);
        buttonList.add(button102);
        buttonList.add(button111);
        buttonList.add(button112);
        buttonList.add(button121);
        buttonList.add(button122);
        buttonList.add(button131);
        buttonList.add(button132);
        buttonList.add(button141);
        buttonList.add(button142);
        buttonList.add(button151);
        buttonList.add(button152);
        if (tournament.isDoubleKO()) {
            buttonList.add(button161);
            buttonList.add(button162);
            buttonList.add(button171);
            buttonList.add(button172);
            buttonList.add(button181);
            buttonList.add(button182);
            buttonList.add(button191);
            buttonList.add(button192);
            buttonList.add(button201);
            buttonList.add(button202);
            buttonList.add(button211);
            buttonList.add(button212);
            buttonList.add(button221);
            buttonList.add(button222);
            buttonList.add(button231);
            buttonList.add(button232);
            buttonList.add(button241);
            buttonList.add(button242);
            buttonList.add(button251);
            buttonList.add(button252);
            buttonList.add(button261);
            buttonList.add(button262);
            buttonList.add(button271);
            buttonList.add(button272);
            buttonList.add(button281);
            buttonList.add(button282);
            buttonList.add(button291);
            buttonList.add(button292);
            buttonList.add(button301);
            buttonList.add(button302);
        }

        for (int i = 0; i < buttonList.size(); i++) {
            buttonList.get(i).setOnAction(new MyButtonHandler());
        }

        if (!tournament.isDoubleKO()) {
            if (tournament.getPlayersInKO() == 8) {
                eightFinalLabel.setVisible(false);
                for (int i = 0; i < 16; i++) {
                    buttonList.get(i).setVisible(false);
                }
            } else if (tournament.getPlayersInKO() == 4) {
                quarterFinalLabel.setVisible(false);
                eightFinalLabel.setVisible(false);
                for (int i = 0; i < 24; i++) {
                    buttonList.get(i).setVisible(false);
                }
            }
        } else {
            if (tournament.getPlayersInKO() == 8) {
                eightFinalLabel.setVisible(false);
                for (int i = 0; i < 24; i++) {
                    buttonList.get(i).setVisible(false);
                }
                for (int i = 32; i < 40; i++) {
                    buttonList.get(i).setVisible(false);
                }
            } else if (tournament.getPlayersInKO() == 4) {
                quarterFinalLabel.setVisible(false);
                eightFinalLabel.setVisible(false);
                for (int i = 0; i < 40; i++) {
                    buttonList.get(i).setVisible(false);
                }
                for (int i = 44; i < 52; i++) {
                    buttonList.get(i).setVisible(false);
                }
            }
        }

        tree = tournament.getTree();
        tree.setTournament(tournament);

        refresh();
    }

    public void refresh() {
        for (int i = 0; i < buttonList.size(); i++) {
            if (i % 2 == 1) {
                buttonList.get(i).setText(tree.getMatch(i / 2).getPlayer1().getFirstName() + " " + tree.getMatch(i / 2).getPlayer1().getLastName() + "    " + tree.getMatch(i / 2).getScore1()); 
            } else {
                buttonList.get(i).setText(tree.getMatch(i / 2).getPlayer2().getFirstName() + " " + tree.getMatch(i / 2).getPlayer2().getLastName() + "    " + tree.getMatch(i / 2).getScore2());
            } 
           /*
            if(tree.getMatch(i / 2).getPlayer1().isDQ()){
              //  tree.getMatch(i / 2).getPlayer1().setFirstName("FREILOS");
              //  tree.getMatch(i / 2).getPlayer1().setLastName("");
              //  tree.enterScore((i / 2), tournament.getFreePassValue(), 0);
            }
            if(tree.getMatch(i / 2).getPlayer2().isDQ()){
              //  tree.getMatch(i / 2).getPlayer2().setFirstName("FREILOS");
              //  tree.getMatch(i / 2).getPlayer2().setLastName("");
              //  tree.enterScore((i / 2), 0, tournament.getFreePassValue());
            } */
        }     
        disableMatches();
    }

    void setup(List<Player> KOPlayers) {
        tree.initialize(KOPlayers);
        refresh();
    }
   /**
    * Diese Methode filtert alle KO-Matches aus die nicht bearbeitbar sein sollen
    */
    public void disableMatches() {                 
      // Blende matches aus die nicht durchgeführt werden dürfen
        for (int i = 0; i < buttonList.size(); i++) {
                if(tree.getMatch(i / 2).getPlayer1().getFirstName().matches("TBD") || tree.getMatch(i / 2).getPlayer2().getFirstName().matches("TBD")
                        || tree.getMatch(i / 2).isFinished()){
                    buttonList.get(i).setDisable(true);
                } else {
                    buttonList.get(i).setDisable(false);
                }   
                // Falls beides Freilose gewinnt eines
                if((tree.getMatch(i / 2).getPlayer1().isDQ() || tree.getMatch(i / 2).getPlayer1().getFirstName().matches("FREILOS")) && 
                   (tree.getMatch(i / 2).getPlayer2().isDQ() || tree.getMatch(i / 2).getPlayer2().getFirstName().matches("FREILOS")) && !tree.getMatch(i / 2).isFinished()){
                   tree.enterScore((i / 2), tournament.getFreePassValue(), 0);
                   tree.getMatch(i / 2).getPlayer2().setLostInKO(tournament, true);
                   tree.getMatch(i / 2).setFinished(true);
                   tree.getMatch(i / 2).getPlayer2().setLostInKO(tournament, true);
                   refresh();
               }
                // Falls Spieler 1 Freilos gewinnt Spieler 2
                if((tree.getMatch(i / 2).getPlayer1().isDQ() || tree.getMatch(i / 2).getPlayer1().getFirstName().matches("FREILOS")) && !tree.getMatch(i / 2).isFinished()
                   && !tree.getMatch(i / 2).getPlayer2().getFirstName().matches("TBD")){
                  // tree.getMatch(i / 2).getPlayer1().setLostInKO(tournament, true);
                   tree.enterScore((i / 2), tournament.getFreePassValue(), 0); 
                   tree.getMatch(i / 2).getPlayer1().setLostInKO(tournament, true);
                   tree.getMatch(i / 2).setFinished(true);
                   tree.getMatch(i / 2).getPlayer1().setLostInKO(tournament, true);
                   refresh();
                }
                // Falls Spieler 2 Freilos gewinnt Spieler 1
                if((tree.getMatch(i / 2).getPlayer2().isDQ() || tree.getMatch(i / 2).getPlayer2().getFirstName().matches("FREILOS")) && !tree.getMatch(i / 2).isFinished()
                   && !tree.getMatch(i / 2).getPlayer1().getFirstName().matches("TBD")){ 
                   tree.getMatch(i / 2).getPlayer2().setLostInKO(tournament, true);
                   tree.enterScore((i / 2), 0, tournament.getFreePassValue());
                   tree.getMatch(i / 2).setFinished(true);
                   refresh();
                }
                
                
        }      
    }

    private class MyButtonHandler implements EventHandler<ActionEvent> {


        @Override
        public void handle(ActionEvent evt) {        
            Button checkButton = (Button) evt.getSource(); 
            /*
            if (checkButton.getText().startsWith("TBD")) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warnung");
                alert.setHeaderText(null);
                alert.setContentText("Dieses Match steht noch nicht fest!");
                alert.showAndWait();
                return;
            } */
            
            // Match auf beendet setzen
            String p1F = "";
            String p1L = "";
            String p2F = "";
            String p2L = "";
            int scoreP1 = 0;
            int scoreP2 = 0;
                    for(int i = 0; i < buttonList.size(); i++){
                        if(buttonList.get(i) == checkButton){
                          p1F = tree.getMatch(i / 2).getPlayer1().getFirstName();
                          p1L = tree.getMatch(i / 2).getPlayer1().getLastName();
                          p2F = tree.getMatch(i / 2).getPlayer2().getFirstName();
                          p2L = tree.getMatch(i / 2).getPlayer2().getLastName();
                        }
                    }
            
            
            TextInputDialog dialog = new TextInputDialog("0");
            dialog.setTitle("Ergebnis eintragen");
            dialog.setHeaderText("Ergebnis eintragen");
            dialog.setContentText(p2F + " " + p2L + ":");
            Button okButton1 = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
            okButton1.setText("Punkte eintragen");
            Button cancelButton1 = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
            cancelButton1.setText("Abbrechen");
            Optional<String> result = dialog.showAndWait();

            if (result.isPresent()) {
                if (result.get().matches("-?[0-9]+")) {
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Ungültige Eingabe!");
                    alert.setContentText("Eintragen der Ergebnise fehlgeschlagen!");
                    alert.showAndWait();
                    return;
                }
            } else {
                return;
            }

            TextInputDialog dialog2 = new TextInputDialog("0");
            dialog2.setTitle("Ergebnis eintragen");
            dialog2.setHeaderText("Ergebnis eintragen");
            dialog2.setContentText(p1F + " " + p1L + ":");
            Button okButton2 = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
            okButton1.setText("Punkte eintragen");
            Button cancelButton2 = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
            cancelButton1.setText("Abbrechen");
            Optional<String> result2 = dialog2.showAndWait();

            if (result2.isPresent()) {
                if (result.get().matches("-?[0-9]+")) {
                    scoreP2 = Integer.parseInt(result.get());
                    scoreP1 = Integer.parseInt(result2.get());
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Ungültige Eingabe!");
                    alert.setContentText("Eintragen der Ergebnise fehlgeschlagen!");
                    alert.showAndWait();
                    return;
                }
            } else {
                return;
            }

            if (result.isPresent() && (scoreP1 != scoreP2)) {
                if (result.get().matches("-?[0-9]+")) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Bestätigung - Punkte eintragen");
                    alert.setHeaderText(p2F + " " + p2L + "       " + scoreP2 + "  :  " + scoreP1 + "      "+ p1F+ " " + p1L);
                    alert.setContentText("Das Ergebnis kann nach Ihrer Bestätigung nicht mehr geändert werden! Fortfahren?");
                    Button okButton3 = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
                    okButton3.setText("Ja");
                    Button cancelButton3 = (Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL);
                    cancelButton3.setText("Abbrechen");

                    Optional<ButtonType> buttonResult = alert.showAndWait();
                    if (buttonResult.get() == ButtonType.OK) {
                        Button button = (Button) evt.getSource();
                        int temp = Integer.parseInt(button.getId().substring(6));
                        tree.enterScore((temp / 10 - 1), Integer.parseInt(result.get()), Integer.parseInt(result2.get())); 
                    } else {
                        return;
                    }
                
                    // Match auf beendet setzen
                    for(int i = 0; i < buttonList.size(); i++){
                        if(buttonList.get(i) == checkButton){
                            tree.getMatch(i / 2).setFinished(true);
                        }
                    }
                            
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Ungültige Eingabe!");
                    alert.setContentText("Eintragen der Ergebnise fehlgeschlagen!");
                    alert.showAndWait();
                    return;
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Fehler");
                alert.setHeaderText(null);
                alert.setContentText("Ein unentschieden ist in der KO-Runde nicht möglich! Eintragen der Ergebnisse fehlgeschlagen!");
                alert.showAndWait();
                return;
            }
            refresh();

        }
    }

}
