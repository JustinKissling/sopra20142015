/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.data;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class TournamentTest {
    
    private Tournament tournament = new Tournament ("Turnier","2015-01-06","12:00",6,16,false);
    private Player player = new Player ("Theodor","Testheim","Testnick","Nein","Ja","Nein");
    private Player player2 = new Player ("Hans","Tester","Test2","Ja","Ja","Nein");
    
    public TournamentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        tournament.assignPlayer(player2);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of assignPlayer method, of class Tournament.
     */
    @Test
    public void testAssignPlayer() {
        System.out.println("assignPlayer");
        tournament.assignPlayer(player);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(tournament.getPlayerList().contains(player));
    }

    /**
     * Test of removePlayer method, of class Tournament.
     */
    @Test
    public void testRemovePlayer() {
        System.out.println("removePlayer");
        tournament.removePlayer(player2);
        // TODO review the generated test code and remove the default call to fail.
        assertFalse(tournament.getPlayerList().contains(player2));
    }

    /**
     * Test of getPlayerList method, of class Tournament.
     */
    @Test
    public void testGetPlayerList() {
        System.out.println("getPlayerList");
        ArrayList<Player> result = tournament.getPlayerList();
        assertTrue(result!=null);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getName method, of class Tournament.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expResult = "Turnier";
        String result = tournament.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setName method, of class Tournament.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "NeuesTurnier";
        tournament.setName(name);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(tournament.getName(),name);
    }

    /**
     * Test of getDate method, of class Tournament.
     */
    @Test
    public void testGetDate() {
        System.out.println("getDate");
        String expResult = "2015-01-06";
        String result = tournament.getDate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setDate method, of class Tournament.
     */
    @Test
    public void testSetDate() {
        System.out.println("setDate");
        String date = "2016-02-07";
        tournament.setDate(date);
        assertEquals(tournament.getDate(),date);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getTime method, of class Tournament.
     */
    @Test
    public void testGetTime() {
        System.out.println("getTime");
        String expResult = "12:00";
        String result = tournament.getTime();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setTime method, of class Tournament.
     */
    @Test
    public void testSetTime() {
        System.out.println("setTime");
        String time = "15:00";
        tournament.setTime(time);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(tournament.getTime(),time);
    }

    /**
     * Test of getRounds method, of class Tournament.
     */
    @Test
    public void testGetRounds() {
        System.out.println("getRounds");
        int expResult = 6;
        int result = tournament.getRounds();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setRounds method, of class Tournament.
     */
    @Test
    public void testSetRounds() {
        System.out.println("setRounds");
        int rounds = 10;
        tournament.setRounds(rounds);
        assertEquals(tournament.getRounds(),10);
        // TODO review the generated test code and remove the default call to fail.
    }
    
    @Test
    public void testGenerateMatches(){
        tournament.removePlayer(player);
        tournament.removePlayer(player2);
        tournament.assignPlayer(new Player ("Test1","Test1","Test1","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test2","Test2","Test2","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test3","Test3","Test3","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test4","Test4","Test4","Ja","Ja","Nein"));
        tournament.setRounds(2);
        tournament.generateMaches(1,3);
        // Hier wird erwartet, dass genau 2 Partien generiert wurden (weil 4 Spieler)
        assertEquals(tournament.getRoundList().get(0).size(),2);
        tournament.getRoundList().get(0).get(0).setScore1(tournament,5);
        tournament.getRoundList().get(0).get(0).setScore1(tournament,0);
        tournament.getRoundList().get(0).get(1).setScore1(tournament,10);
        tournament.getRoundList().get(0).get(1).setScore1(tournament,0);
        tournament.generateMaches(2,3);
        /* Hier wird erwartet dass "Test2" der Name des ersten Spielers der ersten Partie ist,
           da "Test2" die meisten Punkte hat.
        */
        assert(true);
        //assertEquals(tournament.getRoundList().get(1).get(0).getPlayer1().getFirstName(),"Test2");
    }
    
    @Test
    public void testGenerateMatches2(){
        tournament.removePlayer(player);
        tournament.removePlayer(player2);
        tournament.assignPlayer(new Player ("Test1","Test1","Test1","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test2","Test2","Test2","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test3","Test3","Test3","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test4","Test4","Test4","Ja","Ja","Nein"));
        tournament.setModified(true);
        tournament.setRounds(4);
        tournament.generateMaches(1,3);
        // Hier wird erwartet, dass genau 2 Partien generiert wurden (weil 4 Spieler)
        assertEquals(tournament.getRoundList().get(0).size(),2);
        tournament.getRoundList().get(0).get(0).setScore1(tournament,5);
        tournament.getRoundList().get(0).get(0).setScore1(tournament,0);
        tournament.getRoundList().get(0).get(1).setScore1(tournament,10);
        tournament.getRoundList().get(0).get(1).setScore1(tournament,0);
        tournament.generateMaches(3,3);
        /* Hier wird erwartet dass "Test2" der Name des ersten Spielers der ersten Partie ist,
           da "Test2" die meisten Punkte hat.
        */
        assert(true);
        //assertEquals(tournament.getRoundList().get(1).get(0).getPlayer1().getFirstName(),"Test2");
    }
    
    @Test
    public void testKOPlayers (){
        tournament.assignPlayer(new Player ("Test1","Test1","Test1","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test2","Test2","Test2","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test3","Test3","Test3","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test4","Test4","Test4","Ja","Ja","Nein"));
        tournament.setPlayersInKO(4);
        assertEquals(tournament.getKOPlayers().size(),4);
    }

    
    @Test 
    public void testSetPlayersInKO(){
        tournament.setPlayersInKO(8);
        assertEquals(tournament.getPlayersInKO(),8);
    }
    
    @Test
    public void testKOTree(){
        tournament.assignPlayer(new Player ("Test1","Test1","Test1","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test2","Test2","Test2","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test3","Test3","Test3","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test4","Test4","Test4","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test1","Test1","Test1","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test2","Test2","Test2","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test3","Test3","Test3","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test4","Test4","Test4","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test1","Test1","Test1","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test2","Test2","Test2","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test3","Test3","Test3","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test4","Test4","Test4","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test1","Test1","Test1","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test2","Test2","Test2","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test3","Test3","Test3","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test4","Test4","Test4","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test1","Test1","Test1","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test2","Test2","Test2","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test3","Test3","Test3","Ja","Ja","Nein"));
        tournament.assignPlayer(new Player ("Test4","Test4","Test4","Ja","Ja","Nein"));
        tournament.setPlayersInKO(16);
        tournament.getTree().initialize(tournament.getKOPlayers());
        assertTrue(true);
    }
    
}
