/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 *
 * Diese Klasse stellt einen Spieler einer Veranstaltung dar
 * @author admin
 */
public class Player implements Serializable {
    private String firstName;
    private String lastName;
    private String nickName;
    private int startNumber;
    // Hat der Spieler den Turnierbeitrag entrichtet?
    private String paid;
    // Ist der Spieler tatsächlich anwesend?
    private String present;
    // Tabellenstärke (Summe aller Punktzahlen)
    private HashMap<UUID,Integer> tableStrength = new HashMap<UUID,Integer>();
    private HashMap<UUID,Integer> secondaryStrength = new HashMap<UUID,Integer>();
    // Spieler gegen die bereits gespielt wurde
    private HashMap<UUID,ArrayList<Player>> opponents = new HashMap<UUID,ArrayList<Player>>();
    private HashMap<UUID, Boolean> lostInKO = new HashMap<UUID,Boolean>();
    private HashMap<UUID, Boolean> lostDouble = new HashMap<UUID,Boolean>();
    private boolean DQ;
    private int compareStrength;
    private HashMap<UUID, Integer> winPoints = new HashMap<UUID,Integer>();
    private String hasSuperFree;
    
    /**
     * Kontruktor
     * @param firstName
     * @param lastName
     * @param nickName
     * @param present
     * @param paid 
     */
        public Player(String firstName, String lastName, String nickName,String present, String paid, String hasSuperFree) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.present = present;
        this.paid = paid;
        this.DQ = false;
        this.compareStrength = 0;
        this.hasSuperFree = hasSuperFree;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getStartNumber() {
        return startNumber;
    }

    public void setStartNumber(int startNumber) {
        this.startNumber = startNumber;
    }

    public String isPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String isPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }
    
    public void setSuperFree(String hasSuperFree){
        this.hasSuperFree = hasSuperFree;
    }
    
    public String hasSuperFree(){
        return hasSuperFree;
    }
    
    public void addTableStrength(int points, Tournament tournament){

        if (tableStrength.containsKey(tournament.getId())){
        tableStrength.put(tournament.getId(), tableStrength.get(tournament.getId())+points);
        }
    }
    
    public int getTableStrength(Tournament tournament){
        if (tableStrength.containsKey(tournament.getId())){
        return tableStrength.get(tournament.getId());
        }
        return 0;
    }

    
    public void setTableStrength(Tournament tournament){
        this.tableStrength.put(tournament.getId(), 0);
    }
    
    public int getSecondaryStrength(Tournament tournament){
        secondaryStrength.put(tournament.getId(), 0);
        if (!opponents.containsKey(tournament.getId())){
            opponents.put(tournament.getId(), new ArrayList<Player>());
        }
        for (int i=0;i<opponents.get(tournament.getId()).size();i++){
            secondaryStrength.put(tournament.getId(), secondaryStrength.get(tournament.getId())
            +opponents.get(tournament.getId()).get(i).getTableStrength(tournament));
        }
        return secondaryStrength.get(tournament.getId());
    }
    
    public void addOpponent(Player player, Tournament tournament){
        if (opponents.containsKey(tournament.getId())){
        opponents.get(tournament.getId()).add(player);
        } else {
            opponents.put(tournament.getId(), new ArrayList<Player>());
            opponents.get(tournament.getId()).add(player);
        }
    }
    
    public HashMap<UUID,ArrayList<Player>> getOpponents(){
        return opponents;
    }

    public boolean isLostInKO(Tournament tournament) {
        return lostInKO.get(tournament.getId());
    }

    public void setLostInKO(Tournament tournament, boolean lostInKO) {
        this.lostInKO.put(tournament.getId(), lostInKO);
    }

    public boolean isDQ() {
        return DQ;
    }

    public void setDQ(boolean DQ) {
        this.DQ = DQ;
    }

    public boolean isLostDouble(Tournament tournament) {
        return lostDouble.get(tournament.getId());
    }

    public void setLostDouble(Tournament tournament, boolean lostDouble) {
        this.lostDouble.put(tournament.getId(), lostDouble);
    }
    
    public void intTableStrength(Tournament tournament) {
        tableStrength.put(tournament.getId(), 0);
    }
    
    public int getCompareStrength(){
        return compareStrength;
    }
    
    public void setCompareStrength(int i){
        this.compareStrength = i;
    }
    
    public int getWinPoints(Tournament tournament){
        if (winPoints.containsKey(tournament.getId())){
        return winPoints.get(tournament.getId());
        }
        return 0;
    }

    
    public void setWinPoints(Tournament tournament){
        this.winPoints.put(tournament.getId(), 0);
    }
    
    public void addWinPoints(Tournament tournament, int winPoints){
        if (this.winPoints.containsKey(tournament.getId())){
            this.winPoints.put(tournament.getId(), winPoints);
        }
    }

    public String getHasSuperFree() {
        return hasSuperFree;
    }

    public void setHasSuperFree(String hasSuperFree) {
        this.hasSuperFree = hasSuperFree;
    }
    
    
    
    
    
}