package turniermaster.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import turniermaster.data.Match;
import turniermaster.data.Player;
import turniermaster.data.Tournament;

/**
 * Kontrolliert die GUI für die Darstellung eines Turniers
 *
 * @author admin
 */
public class TournamentViewController implements Initializable {

    private Tournament tournament;
    private ArrayList<Player> eventPlayerList = new ArrayList<Player>();
    private ArrayList<String> playerNameList = new ArrayList<String>();
    private ArrayList<RoundViewController> roundControllers = new ArrayList<RoundViewController>();
    // Liste von Möglichkeiten für die Uhrzeitauswahl (Stunden)
    private ObservableList<String> hours = FXCollections.observableArrayList();
    // Liste von Möglichkeiten für die Uhrzeitauswahl (Minuten)
    private ObservableList<String> minutes = FXCollections.observableArrayList();
    private KOController koController;

    @FXML
    private TableColumn firstNameColumn;
    @FXML
    private TableColumn lastNameColumn;
    @FXML
    private TableColumn nickNameColumn;
    @FXML
    private TableColumn startNumberColumn;
    @FXML
    private TableColumn tableStrengthColumn;
    @FXML
    private TableView playerTable;
    @FXML
    private TabPane tournamentPane;
    @FXML
    private TextField infoTournamentName;
    @FXML
    private TextField infoTournamentDate;
    @FXML
    private TextField infoTournamentTime;
    @FXML
    private Button addButton;
    @FXML
    private Button removePlayerButton;
    @FXML
    private Button buttonStartTournament;
    @FXML
    private TableColumn secondaryStrengthColumn;
    @FXML
    private TableColumn winPointsColumn;
    /**
     * Wählt das Turnier für dieses Fenster aus
     *
     * @param tournament
     */
    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
        if(tournament.getStartet()){
            addButton.setDisable(true);
            buttonStartTournament.setDisable(true);
            removePlayerButton.setDisable(true);
        }
        // Spalten mit den Daten des Turniers füllen
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        nickNameColumn.setCellValueFactory(new PropertyValueFactory<>("nickName"));
        startNumberColumn.setCellValueFactory(new PropertyValueFactory<>("startNumber"));
        tableStrengthColumn.setCellValueFactory(new PropertyValueFactory<>("tableStrength"));
        secondaryStrengthColumn.setCellValueFactory(new PropertyValueFactory<>("secondaryStrength"));
        
        tableStrengthColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getTableStrength(tournament));
            }
        });
        
        secondaryStrengthColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getSecondaryStrength(tournament));
            }
        });
        
        winPointsColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getWinPoints(tournament));
            }
        });
        
        if (!tournament.isUsesWinPoints()){
            winPointsColumn.setVisible(false);
        }

        // Update Infoboxen aus dem Verwaltungstab
        infoTournamentName.setText(tournament.getName());
        infoTournamentDate.setText(tournament.getDate());
        infoTournamentTime.setText(tournament.getTime());

        playerTable.getItems().setAll(tournament.getPlayerList());

        // Für jede Runde des Turniers wird ein eigenes Tab geöffnet
        for (int i = 0; i < tournament.getRoundList().size(); i++) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(
                        "RoundView.fxml"));
                Parent root = (Parent) loader.load();
                RoundViewController controller = (RoundViewController) loader.getController();
                controller.setRoundNumber(i + 1);
                controller.setTournament(tournament, this);
                roundControllers.add(controller);
                Tab tab = new Tab(tournament.getRoundList().get(i).getName());
                tab.setContent(root);
                tournamentPane.getTabs().add(tab);
            } catch (IOException ex) {
                Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
                try {
                FXMLLoader loader;
                if (!tournament.isDoubleKO()){
                loader = new FXMLLoader(getClass().getResource(
                        "KO16.fxml"));
                } else {
                loader = new FXMLLoader(getClass().getResource(
                        "DoubleKO16.fxml"));   
                }
                Parent root = (Parent) loader.load();
                koController = loader.getController();
                //controller.setRoundNumber(i + 1);
                //controller.setTournament(tournament, this);
                koController.setTournament(tournament);
                Tab tab = new Tab("Endrunde");
                tab.setContent(root);
                tournamentPane.getTabs().add(tab);
            } catch (IOException ex) {
                Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
            }

    }

    /**
     * Initialisiert den Controller
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Schalte Funktionen für Nicht-Admins aus
        if (LoginWindowController.isAdmin == false) {
            addButton.setDisable(true);
            removePlayerButton.setDisable(true);
        }
        // Die Auswahlmöglichkeiten der Stunden (Uhrzeit) wird initialisiert
        for (int i = 0; i <= 23; i++) {
            hours.add(Integer.toString(i));
        }
        // Die Auswahlmöglichkeiten der Minuten (Uhrzeit) wird initialisiert
        minutes.add("00");
        minutes.add("05");
        for (int i = 10; i <= 55; i += 5) {
            minutes.add(Integer.toString(i));
        }
    }

    /**
     * Fügt einen Spieler aus der Spielerliste der Veranstaltung dem Turnier
     * hinzu
     */
    public void addPlayer() {
        eventPlayerList = new ArrayList<Player>();
        // Nur Spieler die bezahlt haben und anwesend sind dürfen am Turnier teilnehmen
        for(int i = 0; i < MainWindowController.getCurrentEvent().getPlayerList().size(); i++){
            if(MainWindowController.getCurrentEvent().getPlayerList().get(i).isPaid().matches("Ja") && 
                    MainWindowController.getCurrentEvent().getPlayerList().get(i).isPresent().matches("Ja") &&
                    !MainWindowController.getCurrentEvent().getPlayerList().get(i).isDQ()){
                eventPlayerList.add(MainWindowController.getCurrentEvent().getPlayerList().get(i));
            }
        }
        // Wenn der Spieler schon zu diesem Turnier gehört erscheint er nicht mehr in der Auswahl
     /*   for (int i = 0; i < eventPlayerList.size(); i++) {
            if (tournament.getPlayerList().contains(eventPlayerList.get(i))) {
                eventPlayerList.remove(i);
                i--;
            }
        } */
        
        // Wenn der Spieler schon zu diesem Turnier gehört erscheint er nicht mehr in der Auswahl
        for(int i = 0; i < eventPlayerList.size(); i++){
            for(int j = 0; j < tournament.getPlayerList().size(); j++){
                if(eventPlayerList.get(i).getStartNumber() == tournament.getPlayerList().get(j).getStartNumber()){
                    eventPlayerList.remove(i);
                    if (i>0){
                        i = 0;
                    }
                }
            }
        }
        
        playerNameList.clear();
        for (int i = 0; i < eventPlayerList.size(); i++) {
            playerNameList.add(eventPlayerList.
                    get(i).getFirstName() + " " + eventPlayerList.get(i).getLastName());
        }
        // Schauen ob es noch eintragbare Spieler gibt
        if (eventPlayerList.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText(null);
            alert.setContentText("Es gibt keine Spieler die hinzugefügt werden können/dürfen!");
            alert.showAndWait();
            return;
        }
        // Dialog mit allen noch nicht zugehörigen Spielern zur Auswahl
        ChoiceDialog<String> dialog;
        dialog = new ChoiceDialog<>(eventPlayerList.
                get(0).getFirstName() + " " + eventPlayerList.get(0).getLastName(), playerNameList);
        dialog.setTitle("Spieler zuweisen");
        dialog.setHeaderText("Spieler einem Turnier zuweisen");
        dialog.setContentText("Wählen Sie einen Spieler:");
        // Buttons umbenennen/anpassen
        Button cancelButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancelButton.setText("Abbrechen");
        Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        okButton.setText("Eintragen");

        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()) {
            tournament.assignPlayer(eventPlayerList.get(playerNameList.indexOf(result.get())));
            //eventPlayerList.get(playerNameList.indexOf(result.get())).addTableStrength(0);
        }
        playerTable.getItems().setAll(tournament.getPlayerList());

    }

    /**
     * Löscht den ausgewählten Spieler aus diesem Turnier
     */
    public void removePlayer() {
        // Falls Spieler ausgewählt
        if (playerTable.getSelectionModel().getSelectedItem() != null) {
            // Eine Warnmeldung fragt nach einer Bestätigung
            Alert confirmDelete = new Alert(AlertType.CONFIRMATION);
            confirmDelete.setTitle("Löschen bestätigen");
            confirmDelete.setHeaderText(null);
            confirmDelete.setContentText("Wollen sie gen ausgewählten Spieler wirklich aus dem Turnier entfernen?");

            ButtonType buttonAccept = new ButtonType("Ja");
            ButtonType buttonCancel = new ButtonType("Abbrechen");

            confirmDelete.getButtonTypes().setAll(buttonAccept, buttonCancel);

            Optional<ButtonType> result = confirmDelete.showAndWait();
            // Wählt der Benutzer "Ja" aus, so wird der Spieler endgültig gelöscht.
            if (result.get() == buttonAccept) {
                tournament.getPlayerList().remove(playerTable.getSelectionModel().getSelectedItem());
                playerTable.getItems().setAll(tournament.getPlayerList());
            } else {
                confirmDelete.close();
            }
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText(null);
            alert.setContentText("Es ist kein Spieler ausgewählt!");

            alert.showAndWait();
        }
    }

    /**
     * Generiert die Partien der ersten Runde (im Gegensatz zu den anderen
     * Runden geschieht dies zufällig)
     */
    public void generateRoundOne() {
        if (tournament.getPlayerList().size() < tournament.getPlayersInKO()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText("Kann nicht starten");
            alert.setContentText("Nicht genug Spieler für die KO-Runde verfügbar!");
            alert.showAndWait();
        } else {
            for (int i = 0; i < tournament.getPlayerList().size(); i++) {
                if(!tournament.getPlayerList().get(i).isDQ()){
                tournament.getPlayerList().get(i).setTableStrength(tournament);
                tournament.getPlayerList().get(i).setLostDouble(tournament,false);
                tournament.getPlayerList().get(i).setLostInKO(tournament,false);
               } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Kann nicht starten");
                    alert.setContentText("'" + tournament.getPlayerList().get(i).getFirstName() + " " + tournament.getPlayerList().get(i).getLastName() + "'" + " ist durch eine disqualifikation vom Event gebannt worden! Bitte entfernen sie diesen Spieler.");
                    alert.showAndWait();
                    return;
                }
            }
            refresh();
            generateMatches(1);
            tournament.setStarted(true);
            buttonStartTournament.setDisable(true);
            addButton.setDisable(true);
            removePlayerButton.setDisable(true);
        }
    }

    /**
     * Generiert die Partien für eine bestimmte Runde
     *
     * @param round
     */
    public void generateMatches(int round) {
        if (round>tournament.getRounds()){
            koController.setup(tournament.getKOPlayers());
        } else {
        tournament.generateMaches(round, tournament.getFreePassValue());
        roundControllers.get(round - 1).refresh();
        }
    }

    /**
     * Erneuert die Darstellung der Spielerliste
     */
    public void refresh() {
        playerTable.getItems().setAll(tournament.getPlayerList());
    }

    public void disqualify() {
        Player player = (Player) playerTable.getSelectionModel().getSelectedItem();
        if (player != null && !player.isDQ()) {
            Alert alert1 = new Alert(AlertType.CONFIRMATION);
            alert1.setTitle("Spieler disqualifizieren");
            alert1.setHeaderText(null);
            alert1.setContentText("Sind Sie sicher, dass Sie diesen Spieler disqualifizieren wollen?");
            Button okButton1 = (Button) alert1.getDialogPane().lookupButton(ButtonType.OK);
            okButton1.setText("Ja");
            Button cancelButton1 = (Button) alert1.getDialogPane().lookupButton(ButtonType.CANCEL);
            cancelButton1.setText("Abbrechen");

            Optional<ButtonType> result = alert1.showAndWait();
            if (result.get() == ButtonType.OK) {
                tournament.disqualify((Player) playerTable.getSelectionModel().getSelectedItem());
            } else {
                return;
            }
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText(null);
            alert.setContentText("Es ist kein Spieler ausgewählt oder er ist schon disqualifiziert worden!");

            alert.showAndWait();
        }
        refresh();
        for (int i = 0; i < roundControllers.size(); i++) {
            roundControllers.get(i).refresh();
        }

    }

}
