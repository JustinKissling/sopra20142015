package turniermaster.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

/**
 * Diese Klasse stellt ein Turnier dar. Zu jeder Veranstaltung kann es eines
 * oder mehrere geben.
 *
 * @author admin
 */
public class Tournament implements Serializable {

    private String name;
    private String date;
    private String time;
    private int rounds;
    // Name des Kartenspiels
    private String game;
    // Anzahl der Spieler, die in der KO-Runde übrig bleiben (z.B. 8 = KO beginnt mit Viertelfinale)
    private int playersInKO;
    // Hat das Turnier begonnen?
    private boolean started;
    // Punkte für Freilos
    private int freePassValue;
    private UUID id;
    private boolean doubleKO;
    private KOTree tree;
    private boolean modified;
    private boolean usesWinPoints;

    // Liste von Spielern
    private ArrayList<Player> players = new ArrayList<Player>();
    // Liste von Spielern in der KO-Runde
    private List<Player> playersKO = new ArrayList<Player>();
    // Liste von Listen von Partien. Jede Runde hat ihre eigene Liste von Partien.
    private ArrayList<Round> roundList = new ArrayList<Round>();
    // Hilfsliste zur Auslosung von Partien.
    private List<Player> unusedPlayers;

    /**
     * Konstruktor
     *
     * @param name
     * @param date
     * @param time
     * @param rounds
     * @param playersInKO
     * @param doubleKO
     */
    public Tournament(String name, String date, String time, int rounds, int playersInKO, boolean doubleKO) {
        this.name = name;
        this.date = date;
        this.time = time;
        this.rounds = rounds;
        this.playersInKO = playersInKO;
        this.started = false;
        this.id = UUID.randomUUID();
        this.doubleKO = doubleKO;
        this.tree = new KOTree(playersInKO, this);
        this.modified = false;

        // Fügt Listen von Partien für jede Runde des Turniers hinzu
        for (int i = 0; i < rounds; i++) {
            Round round = new Round("Runde " + (i + 1));
            roundList.add(round);
        }

    }

    // Weist Spieler Turnier zu
    public void assignPlayer(Player player) {
        player.intTableStrength(this);
        player.setWinPoints(this);
        players.add(player);
    }

    // Entfernt Spieler von Turnier
    public void removePlayer(Player player) {
        players.remove(player);
    }

    // Gibt die Spielerliste des Turniers zurück
    public ArrayList<Player> getPlayerList() {
        return players;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public boolean isModified() {
        return this.modified;
    }

    /**
     * Generiert Partien für dieses Turnier. Die erste Runde wird zufällig
     * ausgelost, danach spielt immer der beste gegen den zweitbesten, der
     * dritte gegen den vierten usw... Dies wird mit der Tabellenstärke
     * bestimmt.
     *
     * @param round Runde für die die Partien generiert werden sollen. 1 =
     * zufällig, >1 = nach Tabellenstärke
     *
     */
    public void generateMaches(int round, int freePassValue) {

        // Runde 1 nicht neu generieren wenn dies bereits geschehen ist
        if (round == 1 && started) {
            return;
        }

        // Erste Runde wird zufällig bestimmt
        if (round == 1) {
            // Turnier beginnt mit Auslosung
            started = true;
            // Liste mit Spielern die noch keiner Partie zugeteilt sind
            unusedPlayers = new ArrayList<Player>();
            unusedPlayers.addAll(players);
            for (int i=0; i<unusedPlayers.size();i++){
                if (unusedPlayers.get(i).getHasSuperFree().equals("Ja")){
                    Match match = new Match(unusedPlayers.get(i), new Player("SUPERFREILOS", "", "", "", "", ""), 0, freePassValue, this); 
                    unusedPlayers.get(i).setHasSuperFree("Nein");
                    unusedPlayers.remove(i);
                    i--;
                    roundList.get(round - 1).addMatch(match);
                    
                }
            }
            int random;

            // Diese while-Schleife nimmt zwei zufällige Spieler und fügt sie einem Match hinzu
            while (unusedPlayers.size() > 1) {
                random = (int) (Math.random() * unusedPlayers.size());
                Player player1 = unusedPlayers.get(random);
                unusedPlayers.remove(random);
                random = (int) (Math.random() * unusedPlayers.size());
                Player player2 = unusedPlayers.get(random);
                unusedPlayers.remove(random);
                roundList.get(round - 1).addMatch(new Match(player1, player2, (unusedPlayers.size() + 1) / 2, freePassValue, this));

            }

            if (unusedPlayers.size() == 1) {
                Match match = new Match(unusedPlayers.get(0), new Player("FREILOS", "", "", "", "", ""), 0, 0, this);
                //match.setScore1(this, freePassValue);
                roundList.get(round - 1).addMatch(match);
                //match.setFinished(true);
            }

            /*
             Falls es nicht die erste Runde ist, richten sich die Partien nach der Tabellenstärke
             Erster gegen Zweiten, Dritter gegen Vierten usw...
             */
        } else if (!modified) {

            unusedPlayers = new ArrayList<Player>();
            unusedPlayers.addAll(players);
            // Liste von Spielern wird nach ihrer Tabellenstärke sortiert
            for (int i = 0; i < unusedPlayers.size(); i++) {
                unusedPlayers.get(i).setCompareStrength(unusedPlayers.get(i).getTableStrength(this));
            }
            Collections.sort(unusedPlayers, Comparator.comparing(Player::getCompareStrength));

            // Alle Spieler die den Schnitt nicht geschafft haben werden rausgeworfen
            if (round == this.rounds + 1) {
                for (int i = 0; i < unusedPlayers.size(); i++) {
                    if (unusedPlayers.get(i).isDQ()) {
                        unusedPlayers.remove(i);
                        i--;
                    }
                }
                for (int i = playersInKO; i < unusedPlayers.size(); i++) {
                    unusedPlayers.get(i).setLostInKO(this, true);
                    unusedPlayers.get(i).setLostDouble(this, true);
                }

            }

            for (int i = 0; i < unusedPlayers.size(); i++) {
                if (unusedPlayers.get(i).isDQ()) {
                    unusedPlayers.remove(i);
                    i--;

                }
            }

            // Jeweils zwei Spieler werden in einem Match zusammengefasst
            while (unusedPlayers.size() > 1) {
                Player player1 = unusedPlayers.get(unusedPlayers.size() - 1);
                Player player2 = unusedPlayers.get(unusedPlayers.size() - 2);

                unusedPlayers.remove(unusedPlayers.size() - 1);
                unusedPlayers.remove(unusedPlayers.size() - 1);

                roundList.get(round - 1).addMatch(new Match(player1, player2, (unusedPlayers.size() + 1) / 2, freePassValue, this));

            }

            if (unusedPlayers.size() == 1) {
                Match match = new Match(unusedPlayers.get(0), new Player("FREILOS", "", "", "", "", ""), 0, freePassValue, this);
                //match.setScore1(this, freePassValue);
                //match.setFinished(true);
                roundList.get(round - 1).addMatch(match);
            }

        } else {
            unusedPlayers = new ArrayList<Player>();
            unusedPlayers.addAll(players);
            Collections.sort(unusedPlayers, Comparator.comparing(Player::getStartNumber));
            for (int i = 0; i < unusedPlayers.size(); i++) {
                if (unusedPlayers.get(i).isDQ()){
                    unusedPlayers.remove(i);
                    i=0;
                }
            }
            for (int i = 0; i < unusedPlayers.size(); i++) {
                unusedPlayers.get(i).setCompareStrength(unusedPlayers.get(i).getTableStrength(this));
            }
            Collections.sort(unusedPlayers, Comparator.comparing(Player::getCompareStrength));
            int t = 0;
            while (unusedPlayers.size() >= 1) {
                Player player1 = unusedPlayers.get(unusedPlayers.size() - 1);
                int index = findOpponent(unusedPlayers);
                if (unusedPlayers.size() > 1) {
                    Player player2 = unusedPlayers.get(index);
                    unusedPlayers.remove(unusedPlayers.size() - 1);
                    unusedPlayers.remove(index);
                    Match match = new Match(player1, player2, t, freePassValue, this);
                    roundList.get(round - 1).addMatch(match);
                    t++;
                } else {
                    unusedPlayers.remove(unusedPlayers.get(0));
                    Match match = new Match(player1,new Player("FREILOS", "", "", "", "", ""),t,freePassValue,this);
                    //match.setScore1(this, freePassValue);
                    //match.setFinished(true);
                    roundList.get(round - 1).addMatch(match);
                    t++;
                }
            }

        }

    }

    private int findOpponent(List<Player> players) {
        Player player1 = players.get(players.size() - 1);
        int index = -1;
        for (int j = 0; j < player1.getTableStrength(this) + 1; j++) {
            for (int i = 0; i < players.size() - 1; i++) {
                if (players.get(i).getTableStrength(this) == player1.getTableStrength(this) - j) {
                    index = i;
                    break;  
                }

            }
            if (index != -1) {
                return index;
            }
        }
        return 0;
    }

    public ArrayList<Round> getRoundList() {
        return roundList;
    }

    public void setRoundList(ArrayList<Round> roundList) {
        this.roundList = roundList;
    }

    public int getPlayersInKO() {
        return playersInKO;
    }

    public void setPlayersInKO(int playersInKO) {
        this.playersInKO = playersInKO;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * Disqualifiziert einen Spieler aus diesem Turnier
     *
     * @param player
     */
    public void disqualify(Player player) {
        tree.disqualify(player);
        for (int i = 0; i < roundList.size(); i++) {
            for (int j = 0; j < roundList.get(i).size(); j++) {
                Match match = roundList.get(i).get(j);
                if (match.getPlayer1().equals(player)) {
                    match.setScore1(this, 0);
                    match.setScore2(this, freePassValue);
                    match.setFinished(true);
                } else if (match.getPlayer2().equals(player)) {
                    match.setScore1(this, freePassValue);
                    match.setScore2(this, 0);
                    match.setFinished(true);
                }
            }
        }

        player.setNickName(player.getNickName() + " (DQ)");
        player.setDQ(true);
    }

    public int getFreePassValue() {
        return freePassValue;
    }

    public void setFreePassValue(int freePassValue) {
        this.freePassValue = freePassValue;
    }

    public boolean isDoubleKO() {
        return doubleKO;
    }

    public void setDoubleKO(boolean doubleKO) {
        this.doubleKO = doubleKO;
    }

    public List<Player> getKOPlayers() {
        unusedPlayers = new ArrayList<Player>();
        unusedPlayers.addAll(players);
        for (int i = 0; i < unusedPlayers.size(); i++) {
            unusedPlayers.get(i).setCompareStrength(unusedPlayers.get(i).getSecondaryStrength(this));
            if (unusedPlayers.get(i).isDQ()) {
                unusedPlayers.remove(i);
                i--;
            }
        }
        Collections.sort(unusedPlayers, Collections.reverseOrder(Comparator.comparing(Player::getCompareStrength)));
        if (usesWinPoints) {
            for (int i = 0; i < unusedPlayers.size(); i++) {
                unusedPlayers.get(i).setCompareStrength(unusedPlayers.get(i).getWinPoints(this));
            }
            Collections.sort(unusedPlayers, Collections.reverseOrder(Comparator.comparing(Player::getCompareStrength)));
        }
        for (int i = 0; i < unusedPlayers.size(); i++) {
            unusedPlayers.get(i).setCompareStrength(unusedPlayers.get(i).getTableStrength(this));
        }
        Collections.sort(unusedPlayers, Collections.reverseOrder(Comparator.comparing(Player::getCompareStrength)));
        if (unusedPlayers.size() >= playersInKO) {
            unusedPlayers = new ArrayList(unusedPlayers.subList(0, playersInKO));
        }
        while (unusedPlayers.size()<playersInKO){
            unusedPlayers.add(new Player("FREILOS","","","","",""));
        }
        return unusedPlayers;
    }

    public KOTree getTree() {
        return tree;
    }

    public void setTree(KOTree tree) {
        this.tree = tree;
    }

    public boolean isUsesWinPoints() {
        return usesWinPoints;
    }

    public void setUsesWinPoints(boolean usesWinPoints) {
        this.usesWinPoints = usesWinPoints;
    }

    public boolean getStartet() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }
}
