/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.data;

    import java.io.Serializable;
    import java.util.ArrayList;
/**
 *
 * @author Stefan Basaric
 */
public class AdminPasswords implements Serializable {
    private ArrayList<String> passwordList = new ArrayList<String>();
    private String name;

    public AdminPasswords(ArrayList<String> passwordList){
        this.passwordList = passwordList;
    }
    
    // Fügt Passwort in die Liste hinzu
    public void addPassword(String password){
        passwordList.add(password);
    }
    
    // Entfernt ein Passwort aus der Passwortliste
    public void removePassword(int index){
        for(int i = 0; i < passwordList.size(); i++){
            passwordList.remove(index);
        }
    }
    
    // Gibt die Liste zurück
    public ArrayList<String> getPasswordList(){
        return passwordList;
    }
    
    // Gibt name des Passworts zurück
    public String getName(){
        return name;
    }
    
    // Setzt den Namen
    public void setName(String name){
        this.name = name;
    }
}
