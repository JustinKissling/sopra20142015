
package turniermaster.gui;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import turniermaster.IO.FileUtil;
import turniermaster.IO.PdfPrinter;
import turniermaster.data.AdminPasswords;
import turniermaster.data.Event;
import turniermaster.data.Player;
import turniermaster.data.Tournament;

/**
 * FXML Controller class
 *
 * Steuert das Hauptfenster der GUI
 *
 * @author admin
 */
public class MainWindowController implements Initializable {

    // Im Programm geöffnete Turniere
    private ArrayList<Tournament> openedTournaments = new ArrayList<>();
    // Im Programm geöffnete Veranstaltungen
    private ObservableList<Event> openedEvents = FXCollections.observableArrayList();
    // Liste von Möglichkeiten für die Spielersuche
    private ObservableList<String> searchChoices = FXCollections.observableArrayList();
    // Liste von Möglichkeiten für die Uhrzeitauswahl (Stunden)
    private ObservableList<String> hours = FXCollections.observableArrayList();
    // Liste von Möglichkeiten für die Uhrzeitauswahl (Minuten)
    private ObservableList<String> minutes = FXCollections.observableArrayList();
    // Liste von Möglichkeiten für die Anzahl der Spieler in der KO-Runde
    private ObservableList<Integer> playersInKO = FXCollections.observableArrayList(4, 8, 16);
    // Gerade aktive Veranstaltung
    private static Event currentEvent;

    /**
     * Um GUI-Elemente von JavaFX mit Hilfe des Quellcodes zu modifizieren muss
     * man zunächst im SceneBuilder rechts unter Code eine fx:id angeben. Mit
     * dieser erstellt man dann ein neues Objekt mit der Annotation "@FXML" und
     * kann nun mit dem Objekt interagieren.
     *
     * Hier werden die GUI-Elemente aus der .fxml definiert, mit denen durch den
     * Quellcode interagiert werden muss
     */
    @FXML
    Parent root;
    @FXML
    private TabPane tournamentTab;
    @FXML
    private Tab playerListTab;
    @FXML
    private Tab eventTab;
    @FXML
    private Tab manageEventTab;
    @FXML
    private TableView playerTable;
    @FXML
    private TableView eventTable;
    @FXML
    private TextField addFirstNameField;
    @FXML
    private TextField addLastNameField;
    @FXML
    private TextField addNickNameField;
    @FXML
    private Label errorLabel;
    @FXML
    private TableColumn firstNameColumn;
    @FXML
    private TableColumn lastNameColumn;
    @FXML
    private TableColumn nickNameColumn;
    @FXML
    private TableColumn startNumberColumn;
    @FXML
    private TableColumn presentColumn;
    @FXML
    private TableColumn paidColumn;
    @FXML
    private TableColumn eventNameColumn;
    @FXML
    private TableColumn eventDateColumn;
    @FXML
    private TableColumn eventTimeColumn;
    @FXML
    private ChoiceBox eventHourBox;
    @FXML
    private ChoiceBox eventMinuteBox;
    @FXML
    private DatePicker eventDatePicker;
    @FXML
    private TextField eventNameField;
    @FXML
    private ChoiceBox tournamentHourBox;
    @FXML
    private ChoiceBox tournamentMinuteBox;
    @FXML
    private DatePicker tournamentDatePicker;
    @FXML
    private TextField tournamentNameField;
    @FXML
    private TextField playerSearchField;
    @FXML
    private CheckBox presentBox;
    @FXML
    private CheckBox paidBox;
    @FXML
    private TextField roundsField;
    @FXML
    private ChoiceBox playerKOBox;
    @FXML
    private TextField infoEventName;
    @FXML
    private TextField infoEventDate;
    @FXML
    private TextField infoEventTime;
    @FXML
    private ChoiceBox playerSearchBox;
    @FXML
    private Button buttonCreateEvent;
    @FXML
    private Button buttonDeleteEvent;
    @FXML
    private Button editEventButton;
    @FXML
    private Button buttonCreateTournament;
    @FXML
    private Button buttonDeleteTournament;
    @FXML
    private Button buttonEditTournament;
    @FXML
    private Button buttonAddPlayer;
    @FXML
    private Button buttonDeletePlayer;
    @FXML
    private TextField freePassValue;
    @FXML
    private MenuItem menuEditEvent;
    @FXML
    private MenuItem menuDeleteEvent;
    @FXML
    private MenuItem menuExportEvent;
    @FXML
    private MenuItem menuEditTournament;
    @FXML
    private MenuItem menuDeleteTournament;
    @FXML
    private MenuItem menuExportTournament;
    @FXML
    private CheckBox doubleKOBox;
    @FXML
    private MenuItem menuAdminPasswords;
    @FXML
    private MenuItem menuExportPasswords;
    @FXML
    private CheckBox modifiedBox;
    @FXML
    private CheckBox winPointsBox;
    @FXML
    private MenuItem menuRemovePasswords;
    @FXML
    private CheckBox superFreeBox;
    @FXML
    private TableColumn superFreeColumn;
    
    /**
     * Tabellen werden initialisiert. Tabs mit allen zugehörigen Turnieren des
     * Events werden geöffnet
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Falls Nutzer kein Admin: Admin-Funktionen ausschalten
        if (LoginWindowController.isAdmin == false) {
            // Eventfunktionen ausschalten
            buttonCreateEvent.setDisable(true);
            buttonDeleteEvent.setDisable(true);
            eventNameField.setDisable(true);
            eventDatePicker.setDisable(true);
            eventMinuteBox.setDisable(true);
            eventHourBox.setDisable(true);
            editEventButton.setDisable(true);
           
            // Turnierfunktionen ausschalten
            tournamentNameField.setDisable(true);
            tournamentDatePicker.setDisable(true);
            tournamentMinuteBox.setDisable(true);
            tournamentHourBox.setDisable(true);
            roundsField.setDisable(true);
            playerKOBox.setDisable(true);
            buttonCreateTournament.setDisable(true);
            buttonEditTournament.setDisable(true);
            buttonDeleteTournament.setDisable(true);
            freePassValue.setDisable(true);

            // Spielerfunktionen ausschalten
            addFirstNameField.setDisable(true);
            addLastNameField.setDisable(true);
            addNickNameField.setDisable(true);
            presentBox.setDisable(true);
            paidBox.setDisable(true);
            buttonAddPlayer.setDisable(true);
            buttonDeletePlayer.setDisable(true);
            superFreeBox.setDisable(true);
            
            // Menüfunktionen ausschalten
            menuEditEvent.setDisable(true);
            menuDeleteEvent.setDisable(true);
            menuEditTournament.setDisable(true);
            menuDeleteTournament.setDisable(true);
            menuAdminPasswords.setDisable(true);
            menuExportPasswords.setDisable(true);
            menuRemovePasswords.setDisable(true);
        }

        eventTable.setOnMousePressed((e) -> {
            if (e.getClickCount() == 2) {
                selectEvent();
            }
        });

        playerTable.setOnMousePressed((e) -> {
            if (LoginWindowController.isAdmin == true) {
                if (e.getClickCount() == 2) {
                    editPlayer();
                }
            }
        });

        /*
         Hier werden die Spalten der Tabelle in der GUI mit der Tabelle der
         Event-Klasse verlinkt, so dass sich Veränderungen in der Tabelle auch
         auf die GUI auswirken.
         */
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        nickNameColumn.setCellValueFactory(new PropertyValueFactory<>("nickName"));
        startNumberColumn.setCellValueFactory(new PropertyValueFactory<>("startNumber"));
        presentColumn.setCellValueFactory(new PropertyValueFactory<>("present"));
        paidColumn.setCellValueFactory(new PropertyValueFactory<>("paid"));
        superFreeColumn.setCellValueFactory(new PropertyValueFactory<>("hasSuperFree"));

        eventNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        eventDateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        eventTimeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
        eventTable.setItems(openedEvents);

        // Die Auswahlmöglichkeiten der Stunden (Uhrzeit) wird initialisiert
        for (int i = 0; i <= 23; i++) {
            hours.add(Integer.toString(i));
        }

        // Die Auswahlmöglichkeiten der Minuten (Uhrzeit) wird initialisiert
        minutes.add("00");
        minutes.add("05");
        for (int i = 10; i <= 55; i += 5) {
            minutes.add(Integer.toString(i));
        }

        // ChoiceBox für die Spielersuche füllen
        searchChoices.add("Vorname");
        searchChoices.add("Nachname");
        searchChoices.add("Nickname");
        playerSearchBox.setItems(searchChoices);
        playerSearchBox.setValue("Vorname");

        eventHourBox.setValue("0");
        eventMinuteBox.setValue("00");
        tournamentHourBox.setValue("0");
        tournamentMinuteBox.setValue("00");

        tournamentHourBox.setItems(hours);
        tournamentMinuteBox.setItems(minutes);
        eventHourBox.setItems(hours);
        eventMinuteBox.setItems(minutes);
        playerListTab.setDisable(true);
        manageEventTab.setDisable(true);

        playerKOBox.setItems(playersInKO);
        playerKOBox.setValue(4);

    }

    /**
     * Beendet die Anwendung
     */
    public void close() {
        System.exit(1);
    }

    /**
     * Öffnet das Unterfenster zum Spieler hinzufügen
     *
     * @throws IOException
     */
    public void addPlayer() throws IOException {
        addFirstNameField.setStyle("-fx-background-color:white");
        addLastNameField.setStyle("-fx-background-color:white");
        boolean playerAlreadyExists = false;
        ArrayList<Player> playerList = currentEvent.getPlayerList();
        for(int i = 0; i < playerList.size(); i++){
            if(addFirstNameField.getText().matches(playerList.get(i).getFirstName())
            && addLastNameField.getText().matches(playerList.get(i).getLastName())
            && addNickNameField.getText().matches(playerList.get(i).getNickName())){
                playerAlreadyExists = true;
            }
        }
        
        String present = "";
        String paid = "";
        String superFree = "";
        if (presentBox.isSelected()) {
            present = "Ja";
        } else {
            present = "Nein";
        }
        if (paidBox.isSelected()) {
            paid = "Ja";
        } else {
            paid = "Nein";
        }
        if (superFreeBox.isSelected()){
            superFree = "Ja";
        } else {
            superFree = "Nein";
        }
        String addNickName = "";
        if (addNickNameField.getText().matches("")){
            addNickNameField.setText("-");
        }
        if (addFirstNameField.getText().matches("[a-zA-Z-ääööüüÄÄÖÖÜÜß]+$")
                && addLastNameField.getText().matches("[a-zA-Z-ääööüüÄÄÖÖÜÜß]+$") && !playerAlreadyExists) {
            currentEvent.getPlayerList().add(new Player(addFirstNameField.getText(),
                    addLastNameField.getText(), addNickNameField.getText(),
                    present, paid, superFree));
            errorLabel.setText("");
            addFirstNameField.setText("");
            addLastNameField.setText("");
            addNickNameField.setText("");
            presentBox.setSelected(false);
            paidBox.setSelected(false);
            superFreeBox.setSelected(false);
            playerSearchField.setText("");
            playerTable.getItems().setAll(currentEvent.getPlayerList());
            createStartNumbers();

            /*
             Falls eine Eingabe ungültig ist wird das entsprechende Textfeld
             rot markiert und eine Fehlermeldung wird ausgegeben.
             */
        } else if (playerAlreadyExists){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText("Spieler existiert schon!");
            alert.setContentText("Es existiert bereits ein Spieler mit solchen Daten!");
            alert.showAndWait();
        } else {
            if (!addFirstNameField.getText().matches("[a-zA-Z-ääööüüÄÄÖÖÜÜß]+$")) {
                addFirstNameField.setStyle("-fx-background-color:red");
            }
            if (!addLastNameField.getText().matches("[a-zA-Z-ääööüüÄÄÖÖÜÜß]+$")) {
                addLastNameField.setStyle("-fx-background-color:red");
            }
            
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText("Ungültige Eingabe!");
            alert.setContentText("Keine Sonderzeichen oder Leerstellen beim Vor- und Nachnamen erlaubt! Bitte rot markierte Felder verbessern!");
            alert.showAndWait();
        }
    }

    /**
     * Gibt die aktive Veranstaltung zurück
     *
     * @return
     */
    public static Event getCurrentEvent() {
        return currentEvent;
    }

    /**
     * Diese Methode generiert Startnummern für alle angemeldeten Spieler,
     * angefangen bei 1
     */
    public void createStartNumbers() {
        int indexCounter = -1;
        for (int i = 0; i < currentEvent.getPlayerList().size(); i++) {
            indexCounter++;
        }
        // Startnummer eintragen und dann die minimal Erlaubte um 1 erhöhen
        currentEvent.getPlayerList().get(indexCounter).setStartNumber(currentEvent.getMinStartNumber());
        currentEvent.setMinStartNumber(currentEvent.getMinStartNumber() + 1);
        // Liste erneuern
        firstNameColumn.setVisible(false);
        firstNameColumn.setVisible(true);
    }

    /**
     * Disqualifiziert einen Spieler vom Event
     */
    public void disqualifyPlayer(){
        // Spieler wählen
        Player player = (Player) playerTable.getSelectionModel().getSelectedItem();
        // Falls einer gewählt
        if (player != null && !player.isDQ()) {
            Alert confirmDelete = new Alert(AlertType.CONFIRMATION);
            confirmDelete.setTitle("Spieler disqualifizieren");
            confirmDelete.setHeaderText("Disqualifizieren bestätigen");
            confirmDelete.setContentText("Wollen sie den ausgewählten Spieler wirklich disqualifizieren?");

            ButtonType buttonAccept = new ButtonType("Ja");
            ButtonType buttonCancel = new ButtonType("Abbrechen");

            confirmDelete.getButtonTypes().setAll(buttonAccept, buttonCancel);

            Optional<ButtonType> result = confirmDelete.showAndWait();
            // Falls bestätigt
            if (result.get() == buttonAccept) {
                // Disqualifiziere Spieler und mache dies sichtbar
                player.setDQ(true);
                player.setNickName(player.getNickName() + " DQ");
                // Starte Event neu
                selectEvent();
                // Zum "Spielerliste" Tab gehen
                SingleSelectionModel<Tab> selectionModel = tournamentTab.getSelectionModel();
                selectionModel.select(2);
            } 
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText(null);
            alert.setContentText("Es ist kein Spieler ausgewählt oder es ist ein schon disqualifizierter Spieler ausgewählt worden!");

            alert.showAndWait();
        }
    }
    
    /**
     * Entfernt einen Spieler aus dem Event
     */
    public void removePlayer() {
        // Falls ein Spieler ausgewählt
        if (playerTable.getSelectionModel().getSelectedItem() != null) {
            // Eine Warnmeldung fragt nach einer Bestätigung
            Alert confirmDelete = new Alert(AlertType.CONFIRMATION);
            confirmDelete.setTitle("Spieler löschen");
            confirmDelete.setHeaderText("Löschen bestätigen");
            confirmDelete.setContentText("Wollen sie den ausgewählten Spieler wirklich löschen?");

            ButtonType buttonAccept = new ButtonType("Ja");
            ButtonType buttonCancel = new ButtonType("Abbrechen");

            confirmDelete.getButtonTypes().setAll(buttonAccept, buttonCancel);

            Optional<ButtonType> result = confirmDelete.showAndWait();
            // Wählt der Benutzer "Ja" aus, so wird geschaut ob der Spieler in einem Turnier eingetragen ist
            if (result.get() == buttonAccept) {  
                boolean isInTournament = false;
                ArrayList<Tournament> testTournament = currentEvent.getTournamentList();
                for(int i = 0; i < testTournament.size(); i++){
                    for(int j = 0; j < testTournament.get(i).getPlayerList().size(); j++){
                        Player player = (Player) playerTable.getSelectionModel().getSelectedItem();
                        if(player.getStartNumber() == testTournament.get(i).getPlayerList().get(j).getStartNumber()){
                        isInTournament = true;
                       }
                   }
               }              
                
            // Falls Spieler in mind. einem Turnier eingetragen ist 
            if(isInTournament){           
                   
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warnung");
                alert.setHeaderText("Spieler ist in mindestens einem Turnier eingetragen!");
                alert.setContentText("Wenn sie fortfahren wird der Spieler aus dem Event und jedem Turnier in dem er eingetragen ist gelöscht! Fortfahren?");
                    
                ButtonType buttonAccept2 = new ButtonType("Ja");
                ButtonType buttonCancel2 = new ButtonType("Abbrechen");
                alert.getButtonTypes().setAll(buttonAccept2, buttonCancel2);
                    
                Optional<ButtonType> result2 = alert.showAndWait();
              
              // Falls löschen des Spielers in allen Turnieren und Event bestätigt wird
              if (result2.get() == buttonAccept2) {   
                // Durchsuche alle Spielerlisten aller Turniere und lösche den Spieler
                ArrayList<Tournament> tournament = currentEvent.getTournamentList();
                for(int i = 0; i < tournament.size(); i++){
                    for(int j = 0; j < tournament.get(i).getPlayerList().size(); j++){
                        Player player = (Player) playerTable.getSelectionModel().getSelectedItem();
                        if(player.getStartNumber() == tournament.get(i).getPlayerList().get(j).getStartNumber()){
                        tournament.get(i).getPlayerList().remove(tournament.get(i).getPlayerList().get(j));
                       }
                   }
               }
                // Lösche ihn dann aus dem Event und aktualisiere alle Listen
                currentEvent.getPlayerList().remove(playerTable.getSelectionModel().getSelectedItem());
                playerTable.getItems().setAll(currentEvent.getPlayerList());
                playerSearchField.setText("");
                selectEvent();
                
                // Zum "Spielerliste" Tab gehen
                SingleSelectionModel<Tab> selectionModel = tournamentTab.getSelectionModel();
                selectionModel.select(2);
                // Falls löschen des Spielers aus allen Turnieren und Event abgebrochen wird breche Methode ab
              } else {
                  return;
              }
              // Falls Spieler in keinem Turnier angemeldet ist lösche den Spieler
               } else {
                   currentEvent.getPlayerList().remove(playerTable.getSelectionModel().getSelectedItem());
                   playerTable.getItems().setAll(currentEvent.getPlayerList());
                   playerSearchField.setText("");
                } 
                // Falls erste Bestätigungsmeldung abgebrochen wird
            } else {
                confirmDelete.close();
            }
        } // Falls kein Spieler ausgewählt
        else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText(null);
            alert.setContentText("Es ist kein Spieler ausgewählt!");

            alert.showAndWait();
        }
    }

    /**
     * Erstellt eine neue Veranstaltung auf Basis der vom Benutzer eingegebenen
     * Daten.
     */
    public void createEvent() {
        // Farbe der Felder zurücksetzen falls keine Fehler mehr vorhanden
        eventDatePicker.setStyle("-fx-background-color:white");
        eventNameField.setStyle("-fx-background-color:white");
        // Veranstaltung erstellen
        if (eventDatePicker.getValue() != null && eventNameField.getText() != "") {
            openedEvents.add(new Event(eventNameField.getText(),
                    eventDatePicker.getValue().toString(),
                    (String) eventHourBox.getValue() + ":" + (String) eventMinuteBox.getValue(), 1));
            // Felder zurücksetzen
            eventNameField.setText("");
            eventHourBox.setValue("0");
            eventMinuteBox.setValue("00");
            eventDatePicker.setValue(null);
        } else {
            if (eventDatePicker.getValue() == null) {
                eventDatePicker.setStyle("-fx-background-color:red");
            }
            if (eventNameField.getText().equals("")) {
                eventNameField.setStyle("-fx-background-color:red");
            }
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText("Ungültige Eingabe!");
            alert.setContentText("Keine Sonderzeichen oder Leerstellen erlaubt! Bitte rot markierte Felder verbessern!");
            alert.showAndWait();
        }

    }

    /**
     * Die Eigenschaften eines Events werden mit dieser Methode bearbeitet z.B.
     * Name, Datum und Uhrzeit
     */
    public void editEvent() {
        if (eventTable.getSelectionModel().getSelectedItem() != null) {
            // Eigenschaften vom Spieler aufrufen
            Event event = (Event) eventTable.getSelectionModel().getSelectedItem();
            String editEventName = event.getName();
            String editEventDate = event.getDate();
            String editEventTime = event.getTime();

            // Uhrzeit in HH:mm splitten
            String[] splitTime = editEventTime.split(":");

            // Dialog mit Buttons erstellen
            Dialog dialog = new Dialog<>();
            dialog.setTitle("Veranstaltung Bearbeiten");
            dialog.setHeaderText("Daten von " + "'" + event.getName() + "'" + " bearbeiten.");
            ButtonType confirmEdit = new ButtonType("Änderung Speichern");
            ButtonType cancelEdit = new ButtonType("Abbrechen", ButtonData.CANCEL_CLOSE);
            dialog.getDialogPane().getButtonTypes().addAll(confirmEdit, cancelEdit);

            // Erstellen der GridPane
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(50, 150, 10, 10));

            // Erstellen von Elementen
            TextField editEventNameField = new TextField();
            editEventNameField.setPromptText(event.getName());
            editEventNameField.setText(event.getName());
            DatePicker editEventDatePicker = new DatePicker();
            editEventDatePicker.setPromptText(event.getDate());
            editEventDatePicker.setEditable(false);
            ChoiceBox editEventHours = new ChoiceBox();
            editEventHours.setItems(hours);
            editEventHours.setValue(splitTime[0]);
            ChoiceBox editEventMinutes = new ChoiceBox();
            editEventMinutes.setItems(minutes);
            editEventMinutes.setValue(splitTime[1]);

            // Platzieren der Element auf Pane
            grid.add(new Label("Name:"), 0, 0);
            grid.add(editEventNameField, 1, 0);
            grid.add(new Label("Datum:"), 0, 1);
            grid.add(editEventDatePicker, 1, 1);
            grid.add(new Label("Uhrzeit (HH):"), 0, 2);
            grid.add(editEventHours, 1, 2);
            grid.add(new Label("Uhrzeit (mm):"), 0, 3);
            grid.add(editEventMinutes, 1, 3);

            // Hinzufügen der Elemente auf GridPane und Anzeigen vom Dialog
            dialog.getDialogPane().setContent(grid);
            Optional<ButtonType> result = dialog.showAndWait();

            // Wenn Änderung bestätigt wird
            if (result.get() == confirmEdit) {
                if (editEventDatePicker.getValue() != null && eventNameField.getText() != "") {
                    // Werte ändern
                    event.setName(editEventNameField.getText());
                    event.setDate(editEventDatePicker.getValue().toString());
                    event.setTime((String) editEventHours.getValue() + ":" + (String) editEventMinutes.getValue());

                    // Info Textfelder ausfüllen
                    infoEventName.setText(editEventNameField.getText());
                    infoEventDate.setText(editEventDatePicker.getValue().toString());
                    infoEventTime.setText((String) editEventHours.getValue() + ":" + (String) editEventMinutes.getValue());

                    // Bearbeitetes Event auswählen und Dialog schließen
                    openedEvents.set(openedEvents.indexOf(event), event);
                    
                    // Zum "Veranstaltung auswählen" Tab gehen
                    SingleSelectionModel<Tab> selectionModel = tournamentTab.getSelectionModel();
                    selectionModel.select(0);
                    
                    // Ausgewähltes Event "null setzen" und die Tabs deaktivieren
                    currentEvent = null;
                    playerListTab.setDisable(true);
                    manageEventTab.setDisable(true);
                    tournamentTab.getTabs().remove(3, tournamentTab.getTabs().size());
                    
                    dialog.close();
                } else {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Ungültige Eingabe!");
                    alert.setContentText("Leere Felder bitte ausfüllen! Änderung Fehlgeschlagen!");
                    alert.showAndWait();
                }
            }
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText(null);
            alert.setContentText("Es ist kein Event zum bearbeiten ausgewählt!");

            alert.showAndWait();
        }

    }

    /**
     * Macht die ausgewählte Veranstaltung zur aktiven Veranstaltung. Dadurch
     * werden die Spieler und Turniere dieser Veranstaltung geladen und in Tabs
     * geöffnet
     */
    public void selectEvent() {
        // Ist etwas ausgewählt? Dann mache Tabs aktiv
        if (eventTable.getSelectionModel().getSelectedItem() != null) {
            currentEvent = (Event) eventTable.getSelectionModel().getSelectedItem();
            playerListTab.setDisable(false);
            manageEventTab.setDisable(false);
            playerTable.getItems().setAll(currentEvent.getPlayerList());

            // Daten des ausgewählen Events auslesen
            Event event = (Event) eventTable.getSelectionModel().getSelectedItem();
            String writeInfoName = event.getName();
            String writeInfoDate = event.getDate();
            String writeInfoTime = event.getTime();

            // Uhrzeit in HH:mm splitten
            String[] splitTime = writeInfoTime.split(":");

            // Info Textfelder ausfüllen
            infoEventName.setText(writeInfoName);
            infoEventDate.setText(writeInfoDate);
            infoEventTime.setText(splitTime[0] + ":" + splitTime[1]);

        }

        // Alle eventuell von der letzten Veranstaltung geöffneten Tabs verwerfen
        tournamentTab.getTabs().remove(3, tournamentTab.getTabs().size());

        /*
         Für jedes Turnier der nun aktiven Veranstaltung wird ein eigenes Tab geöffnet.
         Die Darstalltung der Tabs übernimmt die "TournamentViewController" Klasse
         */
        for (int i = 0; i < currentEvent.getTournamentList().size(); i++) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(
                        "TournamentView.fxml"));
                Parent root = (Parent) loader.load();
                TournamentViewController controller = (TournamentViewController) loader.getController();
                controller.setTournament(currentEvent.getTournamentList().get(i));
                Tab tab = new Tab(currentEvent.getTournamentList().get(i).getName());
                tab.setContent(root);
                tournamentTab.getTabs().add(tab);
            } catch (IOException ex) {
                Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // Springe zum "Veranstaltung verwalten" Tab
        tournamentTab.getSelectionModel().select(manageEventTab);
    }

    /**
     * Löscht eine Veranstaltung
     */
    public void deleteEvent() {
        if (eventTable.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText(null);
            alert.setContentText("Es ist kein Event zum löschen ausgewählt!");

            alert.showAndWait();
        } else {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Löschen bestätigen");
            alert.setHeaderText(null);
            alert.setContentText("Wollen sie das ausgewählte Event wirklich löschen?");
            Button okButton1 = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
            okButton1.setText("Löschen");
            Button cancelButton1 = (Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL);
            cancelButton1.setText("Abbrechen");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                // Zum "Veranstaltung auswählen" Tab gehen
                    SingleSelectionModel<Tab> selectionModel = tournamentTab.getSelectionModel();
                    selectionModel.select(0);
                // Ausgewähltes Event "null setzen" und die Tabs deaktivieren
                    currentEvent = null;
                    playerListTab.setDisable(true);
                    manageEventTab.setDisable(true);
                    tournamentTab.getTabs().remove(3, tournamentTab.getTabs().size());
                    openedEvents.remove(eventTable.getSelectionModel().getSelectedItem());
            }
        }
    }

    /**
     * Exportiert eine Veranstaltung als Binärdatei Der Benutzer kann mit Hilfe
     * des FileChoosers den Dateinamen sowie den Speicherort bestimmt. Diese
     * Angaben werden zusammen mit der ausgewählten Veranstaltung der FileUtil
     * Klasse übergeben.
     */
    public void exportEvent() {
        // Falls kein Event ausgewählt Fehler ausgeben
        if (currentEvent == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Event exportieren");
            alert.setHeaderText(null);
            alert.setContentText("Kein Event ausgewählt!");
            alert.showAndWait();
            return;
        }
        // Namen des Events auslesen
        Event event = (Event) eventTable.getSelectionModel().getSelectedItem();
        String eventName = event.getName();

        if (eventTable.getSelectionModel().getSelectedItem() != null) {
            // Dialog zum Aussuchen des Speicherorts und Dateinamens
            FileChooser fileChooser = new FileChooser();
            // Standardverzeichnis auswählen
            fileChooser.setInitialDirectory(
                    new File(System.getProperty("user.home")));
            fileChooser.setTitle("Wählen Sie einen Speicherort");
            fileChooser.setInitialFileName(eventName);
            // Nur Dateien mit der Endung ".trv" zeigen
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Turniermaster Veranstaltung (.vrn)", "*.vrn")
            );
            File file = fileChooser.showSaveDialog((Stage) root.getScene().getWindow());
            if (file != null) {
                FileUtil.exportToFile(eventTable.getSelectionModel().getSelectedItem(), file);
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                alert.setContentText("Veranstaltung erfolgreich exportieret!");

                alert.showAndWait();
            }
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText(null);
            alert.setContentText("Es ist kein Event zum exportieren ausgewählt!");

            alert.showAndWait();
        }
    }

    /**
     * Exportiert ein Turnier als Datei
     */
    public void exportTournament() {
        // Falls kein Event ausgewählt Fehler ausgeben
        if (currentEvent == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Turnier exportieren");
            alert.setHeaderText(null);
            alert.setContentText("Kein Event ausgewählt!");
            alert.showAndWait();
            return;
        }
        // Variablen die fürs Exportieren und Auswählen des Turniers gebraucht werden
        List tournamentNameList = new ArrayList<String>();
        int exportTournamentAtIndex = -1;
        int sizeOfTournamentList = currentEvent.getTournamentList().size();
        // Füllen der ArrayList mit Namen der Turniere des ausgewählten Events
        for (int i = 0; i < currentEvent.getTournamentList().size(); i++) {
            tournamentNameList.add(currentEvent.getTournamentList().get(i).getName());
        }

        // Abfragen ob es überhaupt Turniere gibt
        if (sizeOfTournamentList == 0) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText(null);
            alert.setContentText("Event hat noch keine Turniere die exportiert werden können! Bitte zuerst ein Turnier erstellen!");

            alert.showAndWait();
            return;
        }

        // Erstellen des Dialogs
        ChoiceDialog<String> dialog;
        dialog = new ChoiceDialog<>(currentEvent.getTournamentList().get(0).getName(), tournamentNameList);
        dialog.setTitle("Turnier exportieren");
        dialog.setHeaderText(null);
        dialog.setContentText("Wählen Sie ein Turnier, dass sie exportieren wollen:");

        // Buttons umbenennen/anpassen
        Button cancelButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancelButton.setText("Abbrechen");
        Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        okButton.setText("Exportieren");

        // Dialog Zeichnen
        Optional<String> result = dialog.showAndWait();

        // Wenn in der ChoiceBox etwas ausgewählt
        if (result.isPresent()) {
            // Durchlaufe Array maximal so lange wie es Turniere gibt
            for (int i = 0; i < sizeOfTournamentList; i++) {
                // Erhöhe bei jedem durchlauf Suchvariable um 1
                exportTournamentAtIndex++;
                // Falls das gesuchte Turnier das Ausgewählte ist
                if (currentEvent.getTournamentList().get(i).getName().matches(result.get())) {

                    // Eigentlicher Export ab hier
                    // Dialog zum Aussuchen des Speicherorts und Dateinamens
                    FileChooser fileChooser = new FileChooser();
                    // Standardverzeichnis auswählen
                    fileChooser.setInitialDirectory(
                            new File(System.getProperty("user.home")));
                    fileChooser.setTitle("Wählen Sie einen Speicherort");
                    fileChooser.setInitialFileName(currentEvent.getTournamentList().get(i).getName());
                    // Nur Dateien mit der Endung ".trv" zeigen
                    fileChooser.getExtensionFilters().addAll(
                            new FileChooser.ExtensionFilter("Turniermaster Veranstaltung (.trn)", "*.trn")
                    );
                    File file = fileChooser.showSaveDialog((Stage) root.getScene().getWindow());
                    if (file != null) {
                        FileUtil.exportToFile(currentEvent.getTournamentList().get(exportTournamentAtIndex), file);
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Information");
                        alert.setHeaderText(null);
                        alert.setContentText("Turnier erfolgreich exportieret!");

                        alert.showAndWait();
                        return;
                    }
                }
            }
        }
    }

    /**
     * Importiert eine Veranstaltung aus einer Datei. Die Datei wird mit Hilfe
     * des FileChoosers ausgewählt und dann der FileUtil Klasse übergeben,
     * welche den Import übernimmt.
     */
    public void importEvent() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.setTitle("Wählen Sie eine Datei");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Turniermaster Veranstaltung (.vrn)", "*.vrn")
        );
        File file = fileChooser.showOpenDialog((Stage) root.getScene().getWindow());
        if (file != null && FileUtil.importFromFile(file)!=null&&FileUtil.importFromFile(file).getClass()==Event.class) {
            openedEvents.add((Event) FileUtil.importFromFile(file));
            // Zum "Veranstaltung auswählen" Tab gehen
                SingleSelectionModel<Tab> selectionModel = tournamentTab.getSelectionModel();
                selectionModel.select(0);
            // Ausgewähltes Event "null setzen" und die Tabs deaktivieren
                currentEvent = null;
                playerListTab.setDisable(true);
                manageEventTab.setDisable(true);
                tournamentTab.getTabs().remove(3, tournamentTab.getTabs().size());
        }
    }

    /**
     * Importiert eine einzelnes Turnier aus einer Datei. Die Datei wird mit
     * Hilfe des FileChoosers ausgewählt und dann der FileUtil Klasse übergeben,
     * welche den Import übernimmt.
     */
    public void importTournament() {
         // Falls kein Event ausgewählt Fehler ausgeben
        if (currentEvent == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Turnier importieren");
            alert.setHeaderText(null);
            alert.setContentText("Kein Event ausgewählt!");
            alert.showAndWait();
            return;
        }
        if (currentEvent != null) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(
                    new File(System.getProperty("user.home"))
            );
            fileChooser.setTitle("Wählen Sie eine Datei");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Turniermaster Turnier (.trn)", "*.trn")
            );
            File file = fileChooser.showOpenDialog((Stage) root.getScene().getWindow());
            if (file != null && FileUtil.importFromFile(file)!=null && FileUtil.importFromFile(file).getClass()==Tournament.class) {
                Tournament tournament = (Tournament) FileUtil.importFromFile(file);
               
                // Das ist neu
                ArrayList<Player> playerList = tournament.getPlayerList();
                for(int i = 0; i < playerList.size(); i++){
                    boolean isInTournament = false;
                    boolean isButDifferentStart = false;
                    for(int j = 0; j < currentEvent.getPlayerList().size(); j++){
                        if(playerList.get(i).getFirstName().matches(currentEvent.getPlayerList().get(j).getFirstName()) && 
                           playerList.get(i).getLastName().matches(currentEvent.getPlayerList().get(j).getLastName()) &&
                           playerList.get(i).getNickName().matches(currentEvent.getPlayerList().get(j).getNickName()) &&
                           playerList.get(i).getStartNumber() == currentEvent.getPlayerList().get(j).getStartNumber()){
                           isInTournament = true;
                        }  
                        if(playerList.get(i).getFirstName().matches(currentEvent.getPlayerList().get(j).getFirstName()) && 
                           playerList.get(i).getLastName().matches(currentEvent.getPlayerList().get(j).getLastName()) &&
                           playerList.get(i).getNickName().matches(currentEvent.getPlayerList().get(j).getNickName()) &&
                           !(playerList.get(i).getStartNumber() == currentEvent.getPlayerList().get(j).getStartNumber())){
                           isButDifferentStart = true;
                           if(!isInTournament && isButDifferentStart){
                           playerList.get(i).setNickName(playerList.get(i).getNickName()+"1");
                           }
                        }     
                    }
                    if(!isInTournament && !isButDifferentStart){                          
                        currentEvent.getPlayerList().add(new Player(playerList.get(i).getFirstName(), playerList.get(i).getLastName(), playerList.get(i).getNickName(), playerList.get(i).isPaid(), playerList.get(i).isPresent(), playerList.get(i).hasSuperFree()));                     
                        playerTable.getItems().setAll(currentEvent.getPlayerList());
                        createStartNumbers();
                        playerList.get(i).setStartNumber(currentEvent.getMinStartNumber() - 1);
                    } else if (!isInTournament && isButDifferentStart) {
                        currentEvent.getPlayerList().add(new Player(playerList.get(i).getFirstName(), playerList.get(i).getLastName(), playerList.get(i).getNickName(), playerList.get(i).isPaid(), playerList.get(i).isPresent(), playerList.get(i).hasSuperFree()));                     
                        playerTable.getItems().setAll(currentEvent.getPlayerList());
                        createStartNumbers();
                        playerList.get(i).setStartNumber(currentEvent.getMinStartNumber() - 1);
                    }
                    
                }
                    
                // Hier endet Neu   
                    
                    
                    
                for (int i = 0; i < openedEvents.size(); i++) {
                    for (int j = 0; j < openedEvents.get(i).getTournamentList().size(); j++) {
                        if (openedEvents.get(i).getTournamentList().get(j).getId().compareTo(tournament.getId())==0) {
                            Alert alert = new Alert(AlertType.CONFIRMATION);
                            alert.setTitle("Bestätigung");
                            alert.setHeaderText("Turnier bereits vorhanden");
                            alert.setContentText("Das Turnier, das Sie importieren möchten, ist bereits vorhanden. Überschreiben?");
                            Button cancelButton1 = (Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL);
                            cancelButton1.setText("Abbrechen");
                            Optional<ButtonType> result = alert.showAndWait();
                            if (result.get() == ButtonType.OK) {
                                 openedEvents.get(i).getTournamentList().set(j, tournament);
                            } else {
                                 currentEvent.getTournamentList().add(tournament);
                            }
                            selectEvent();
                            return;
                        }
                    }
                }
                
                currentEvent.getTournamentList().add(tournament);
                selectEvent();
            }
        }
    }

    /**
     * Sucht Spieler in der Spielerliste des Events nach Vor-, Nach- oder
     * Nicknamen und gibt diese in einer Kopie der Spielerliste aus
     */
    public void searchPlayer() throws IOException {

        // Benötigte Variablen (Kopie einer Spielerliste und Zahl für Iteration
        List playersToSearch = new ArrayList<Player>();
        int sizeOfPlayerList = currentEvent.getPlayerList().size();

        // Suche nach Vorname und update die Liste
        if (playerSearchBox.getValue().equals("Vorname")) {
            for (int i = 0; i < sizeOfPlayerList; i++) {
                if (currentEvent.getPlayerList().get(i).getFirstName().contains(playerSearchField.getText())) {
                    playersToSearch.add(currentEvent.getPlayerList().get(i));
                }
            }
        }
        // Suche nach Nachname und update die Liste
        if (playerSearchBox.getValue().equals("Nachname")) {
            for (int i = 0; i < sizeOfPlayerList; i++) {
                if (currentEvent.getPlayerList().get(i).getLastName().contains(playerSearchField.getText())) {
                    playersToSearch.add(currentEvent.getPlayerList().get(i));
                }
            }
        }
        // Suche nach Nickname und Update die Liste
        if (playerSearchBox.getValue().equals("Nickname")) {
            for (int i = 0; i < sizeOfPlayerList; i++) {
                if (currentEvent.getPlayerList().get(i).getNickName().contains(playerSearchField.getText())) {
                    playersToSearch.add(currentEvent.getPlayerList().get(i));
                }
            }
        }
        // Gebe die gesuchten Spieler in einer Kopie der Spielerliste aus
        playerTable.getItems().setAll(playersToSearch);
        // Falls Suchfeld leer wird die orginale Spielerliste angezeigt
        if (playerSearchField.getText().equals("")) {
            playerTable.getItems().setAll(currentEvent.getPlayerList());
        }
    }

    /**
     * Diese Methode zeichnet die Spielerliste falls die ChoiceBox vom Suchfeld
     * geändert wird. Ebenso säubert sie das Suchfeld
     */
    public void refreshPlayerList() {
        playerSearchField.setText("");
        playerTable.getItems().setAll(currentEvent.getPlayerList());
    }

    /**
     * Diese Methode bearbeitet die Eigenschaften eines Spielers. z.B. seinen
     * Namen oder seine Präsens
     */
    public void editPlayer() {
        if (playerTable.getSelectionModel().getSelectedItem() != null) {
            // Eigenschaften vom Spieler aufrufen
            Player player = (Player) playerTable.getSelectionModel().getSelectedItem();
            String editFirstName = player.getFirstName();
            String editLastName = player.getLastName();
            String editNickName = player.getNickName();
            String editPresence = player.isPresent();
            String editPayments = player.isPaid();   
            System.out.println(player.hasSuperFree());
            
            // Es wird geschaut ob der Spieler in einem Turnier eingetragen ist
            
            boolean isInTournament = false;
            ArrayList<Tournament> testTournament = currentEvent.getTournamentList();
            for(int i = 0; i < testTournament.size(); i++){
                for(int j = 0; j < testTournament.get(i).getPlayerList().size(); j++){                   
                    if(player.getStartNumber() == testTournament.get(i).getPlayerList().get(j).getStartNumber()){
                        isInTournament = true;
                   }
               }
           }    
            if(isInTournament){
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warnung");
                alert.setHeaderText(player.getFirstName() + " " + player.getLastName() + " nimmt an mindestens einem Turnier teil.");
                alert.setContentText("Das Ändern der Präsenz und der Abrechnung des Beitrags werden ausgeblendet! Der Vor-, Nach- und Nickname wird für alle Turniere in denen der Spieler eingetragen ist angepasst.");

                alert.showAndWait();
            }
            
            // Dialog mit Buttons erstellen
            Dialog dialog = new Dialog<>();
            dialog.setTitle("Spieler Bearbeiten");
            dialog.setHeaderText("Daten von " + "'" + player.getFirstName() + " " + player.getLastName() + "'" + " bearbeiten.");
            ButtonType confirmEdit = new ButtonType("Änderung Speichern");
            ButtonType cancelEdit = new ButtonType("Abbrechen", ButtonData.CANCEL_CLOSE);
            dialog.getDialogPane().getButtonTypes().addAll(confirmEdit, cancelEdit);

            // Erstellen der GridPane
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(50, 150, 10, 10));

            // Erstellen von Elementen
            TextField editFirstNameTextField = new TextField();
            editFirstNameTextField.setPromptText(player.getFirstName());
            editFirstNameTextField.setText(player.getFirstName());
            TextField editLastNameTextField = new TextField();
            editLastNameTextField.setPromptText(player.getLastName());
            editLastNameTextField.setText(player.getLastName());
            TextField editNickNameTextField = new TextField();
            editNickNameTextField.setPromptText(player.getNickName());
            editNickNameTextField.setText(player.getNickName());
            CheckBox editPresenceBox = new CheckBox();
            CheckBox editPaymentBox = new CheckBox();
            if (player.isPaid().matches("Ja")) {
                editPaymentBox.setSelected(true);
            } else {
                editPaymentBox.setSelected(false);
            }
            if (player.isPresent().matches("Ja")) {
                editPresenceBox.setSelected(true);
            } else {
                editPresenceBox.setSelected(false);
            }

            // Platzieren der Element auf Pane
            grid.add(new Label("Vorname:"), 0, 0);
            grid.add(editFirstNameTextField, 1, 0);
            grid.add(new Label("Nachname:"), 0, 1);
            grid.add(editLastNameTextField, 1, 1);
            grid.add(new Label("Nickname:"), 0, 2);
            grid.add(editNickNameTextField, 1, 2);
            grid.add(new Label("Anwesend:"), 0, 3);
            grid.add(editPresenceBox, 1, 3);
            grid.add(new Label("Bezahlt:"), 0, 4);
            grid.add(editPaymentBox, 1, 4);
                       
            // Hinzufügen der Elemente auf GridPane und Anzeigen vom Dialog
            dialog.getDialogPane().setContent(grid);
            // Falls Spieler in einem Turnier können Wahrheitswerden nicht geändert werden
            if(isInTournament){
                editPresenceBox.setDisable(true);
                editPaymentBox.setDisable(true);
            }
            Optional<ButtonType> result = dialog.showAndWait();
            // Wenn Änderung bestätigt wird
            if (result.get() == confirmEdit) {
            
            // Schaue ob es so einen Spieler nicht schon im Event gibt
            String checkPresence = "";
            String checkPaid = "";
            if (editPresenceBox.isSelected()){
                checkPresence = "Ja";
            } else {
                checkPresence = "Nein";
            }
            if (editPaymentBox.isSelected()){
                checkPaid = "Ja";
            } else {
                checkPaid = "Nein";
            }
            boolean playerAlreadyExists = false;
            boolean changedBooleans = false;
            boolean playerChanged = false;
            ArrayList<Player> playerList = currentEvent.getPlayerList();
            for(int i = 0; i < playerList.size(); i++){
                if(editFirstNameTextField.getText().matches(playerList.get(i).getFirstName())
                && editLastNameTextField.getText().matches(playerList.get(i).getLastName())
                && editNickNameTextField.getText().matches(playerList.get(i).getNickName())){
                    playerAlreadyExists = true;
                } 
                if(!checkPresence.matches(player.isPresent())
                || !checkPaid.matches(player.isPaid())){                   
                    changedBooleans = true;
                }
                if(!editFirstName.matches(editFirstNameTextField.getText())
                   || !editLastName.matches(editLastNameTextField.getText())
                   || !editNickName.matches(editNickNameTextField.getText())){                  
                    playerChanged = true;
                }
            }
                if (editFirstNameTextField.getText().matches("[a-zA-Z-ääööüüÄÄÖÖÜÜß]+$")
                        && editLastNameTextField.getText().matches("[a-zA-Z-ääööüüÄÄÖÖÜÜß]+$") && ((!playerChanged && changedBooleans) || (playerChanged && !changedBooleans)) && !(playerChanged && playerAlreadyExists)) {
                    // Ändere alle Arten von Namen
                    player.setFirstName(editFirstNameTextField.getText());
                    player.setLastName(editLastNameTextField.getText());
                    if(editNickNameTextField.getText().matches("")){
                    player.setNickName("-");
                   } else {
                        player.setNickName(editNickNameTextField.getText());
                    }
                // Falls Spieler in mindestens einem Turnier    
                if(isInTournament){
                    // Ändere alle Arten von Namen in allen Turnieren
                    for(int i = 0; i < currentEvent.getTournamentList().size(); i++){
                        for(int j = 0; j <currentEvent.getTournamentList().get(i).getPlayerList().size(); j++){
                            if (player.getStartNumber() == currentEvent.getTournamentList().get(i).getPlayerList().get(j).getStartNumber()){
                                currentEvent.getTournamentList().get(i).getPlayerList().get(j).setFirstName(player.getFirstName());
                                currentEvent.getTournamentList().get(i).getPlayerList().get(j).setLastName(player.getLastName());
                                currentEvent.getTournamentList().get(i).getPlayerList().get(j).setNickName(player.getNickName());
                            }
                        }
                    }
                }
                    // Ändere Anwesenheit
                    if (editPresenceBox.isSelected() == true) {
                        player.setPresent("Ja");
                    } else {
                        player.setPresent("Nein");
                    }
                    // Ändere ob Spieler bezahlt hat
                    if (editPaymentBox.isSelected() == true) {
                        player.setPaid("Ja");
                    } else {
                        player.setPaid("Nein");
                    }
                    // Tabelle updaten und Dialog schließen
                    playerSearchField.setText("");
                    playerTable.getItems().setAll(currentEvent.getPlayerList());
                    if(isInTournament){
                        // Event neustarten damit alle Tabellen geupdatet werden
                        selectEvent();
                        // Zum "Spielerliste" Tab gehen
                        SingleSelectionModel<Tab> selectionModel = tournamentTab.getSelectionModel();
                        selectionModel.select(2);
                    } 
                    // Dialog schließen
                    dialog.close();
                } else if ((playerChanged && playerAlreadyExists) || (!playerChanged && !changedBooleans)){
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Spieler existiert schon!");
                    alert.setContentText("Ein Spieler mit denselben Daten existiert schon! Änderung Fehlgeschlagen!");
                    alert.showAndWait();
                } else {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Ungültige Eingabe!");
                    alert.setContentText("Keine Sonderzeichen oder Leerstellen beim Vor- und Nachnamen erlaubt! Änderung Fehlgeschlagen!");
                    alert.showAndWait();
                }
            }

        }

    }

    /**
     * Erstellt ein neues Turnier mit Hilfe der vom Benutzer eingegebenen Daten
     * und fügt es der aktuellen Veranstaltung hinzu.
     *
     * @throws IOException
     */
    public void addTournament() throws IOException {
        // Farbe der Felder zurücksetzen falls keine Fehler mehr vorhanden
        tournamentDatePicker.setStyle("-fx-background-color:white");
        tournamentNameField.setStyle("-fx-background-color:white");
        roundsField.setStyle("-fx-background-color:white");
        freePassValue.setStyle("-fx-background-color:white");
        // Angaben gültig? Dann Turnier erstellen
        if (tournamentDatePicker.getValue() != null && tournamentNameField.getText() != "" && roundsField.getText().matches("-?[0-9]+")
                &&freePassValue.getText().matches("-?[0-9]+")) {
            currentEvent.addTournament(tournamentNameField.getText(),
                    tournamentDatePicker.getValue().toString(),
                    (String) tournamentHourBox.getValue() + ":" + (String) tournamentMinuteBox.getValue(),
                    Integer.parseInt(roundsField.getText()), (int) playerKOBox.getValue(), Integer.parseInt(freePassValue.getText()),doubleKOBox.isSelected(),modifiedBox.isSelected(),winPointsBox.isSelected());
            // Felder zurücksetzen
            tournamentNameField.setText("");
            roundsField.setText("");
            freePassValue.setText("");
            tournamentHourBox.setValue("0");
            tournamentMinuteBox.setValue("00");
            tournamentDatePicker.setValue(null);
            modifiedBox.setSelected(false);
            winPointsBox.setSelected(false);
            // Neuen Tab für Turnier öffnen
            selectEvent();
            tournamentTab.getSelectionModel().select(tournamentTab.getTabs()
                    .get(tournamentTab.getTabs().size() - 1));
        } else {
            if (tournamentDatePicker.getValue() == null) {
                tournamentDatePicker.setStyle("-fx-background-color:red");
            }
            if (tournamentNameField.getText().equals("")) {
                tournamentNameField.setStyle("-fx-background-color:red");
            }
            if (!roundsField.getText().matches("-?[0-9]+")) {
                roundsField.setStyle("-fx-background-color:red");
            }
            if (!freePassValue.getText().matches("-?[0-9]+")){
                freePassValue.setStyle("-fx-background-color:red");
            }
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText("Ungültige Eingabe!");
            alert.setContentText("Keine Sonderzeichen oder Leerstellen erlaubt! Bitte rot markierte Felder verbessern!");
            alert.showAndWait();
        }
    }

    /**
     * Entfernt ein Turnier aus der ausgewählten Veranstaltung
     */
    public void removeTournament() {
        // Falls kein Event ausgewählt Fehler ausgeben
        if (currentEvent == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Turnier löschen");
            alert.setHeaderText(null);
            alert.setContentText("Kein Event ausgewählt!");
            alert.showAndWait();
            return;
        }
        // Variablen die fürs Löschen und Auswählen des Turniers gebraucht werden
        List tournamentNameList = new ArrayList<String>();
        int deleteTournamentAtIndex = -1;
        int sizeOfTournamentList = currentEvent.getTournamentList().size();
        // Füllen der ArrayList mit Namen der Turniere des ausgewählten Events
        for (int i = 0; i < currentEvent.getTournamentList().size(); i++) {
            tournamentNameList.add(currentEvent.getTournamentList().get(i).getName());
        }

        // Abfragen ob es überhaupt Turniere gibt
        if (sizeOfTournamentList == 0) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText(null);
            alert.setContentText("Event hat noch keine Turniere die gelöscht werden können! Bitte zuerst ein Turnier erstellen!");

            alert.showAndWait();
            return;
        }

        // Erstellen des Dialogs
        ChoiceDialog<String> dialog;
        dialog = new ChoiceDialog<>(currentEvent.getTournamentList().get(0).getName(), tournamentNameList);
        dialog.setTitle("Turnier löschen");
        dialog.setHeaderText(null);
        dialog.setContentText("Wählen Sie ein Turnier, dass sie löschen wollen:");

        // Buttons umbenennen/anpassen
        Button cancelButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancelButton.setText("Abbrechen");
        Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        okButton.setText("Turnier löschen");

        // Dialog Zeichnen
        Optional<String> result = dialog.showAndWait();

        // Wenn in der ChoiceBox etwas ausgewählt
        if (result.isPresent()) {
            // Durchlaufe Array maximal so lange wie es Turniere gibt
            for (int i = 0; i < sizeOfTournamentList; i++) {
                // Erhöhe bei jedem durchlauf Löschvariable um 1
                deleteTournamentAtIndex++;
                // Falls das gesuchte Turnier das Ausgewählte ist
                if (currentEvent.getTournamentList().get(i).getName().matches(result.get())) {
                    // Lösche Turnier, Update Event und beende die Methode
                    currentEvent.getTournamentList().remove(deleteTournamentAtIndex);
                    selectEvent();
                    return;
                }
            }
        }
    }

    /**
     * Diese Klasse erstellt eine PDF-Datei von einem Event. Diese PDF-Datei
     * kann dann gedruckt werden.
     */
    public void printEvent() {
        // Falls kein Event ausgewählt Fehler ausgeben
        if (currentEvent == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("PDF erstellen");
            alert.setHeaderText(null);
            alert.setContentText("Kein Event ausgewählt!");
            alert.showAndWait();
            return;
        }
        Event event = (Event) eventTable.getSelectionModel().getSelectedItem();
        String eventName = event.getName();
        if (eventTable.getSelectionModel().getSelectedItem() != null) {
            // Dialog zum Aussuchen des Speicherorts und Dateinamens
            FileChooser fileChooser = new FileChooser();
            // Standardverzeichnis auswählen
            fileChooser.setInitialDirectory(
                    new File(System.getProperty("user.home")));
            fileChooser.setTitle("Wählen Sie einen Speicherort");
            fileChooser.setInitialFileName(eventName);
            // Nur Dateien mit der Endung ".pdf" zeigen
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Turniermaster Veranstaltung (.pdf)", "*.pdf")
            );
            File file = fileChooser.showSaveDialog((Stage) root.getScene().getWindow());
            if (file != null) {
                PdfPrinter printer = new PdfPrinter();
                printer.print(file, (Event) eventTable.getSelectionModel().getSelectedItem());
                // Meldung für erfolgreichen Export wird ausgegeben
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText("Veranstaltung als PDF exportieren.");
                alert.setContentText("PDF-Export erfolgreich durchgeführt!");
                alert.showAndWait();
            }
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText("PDF-Export fehlgeschlagen");
            alert.setContentText("Sie müssen zuerst ein Event auswählen!");
            alert.showAndWait();
        }
    }

    /**
     * Diese Klasse erstellt eine PDF-Datei von einem einzelnen Turnier. Diese
     * PDF-Datei kann dann gedruckt werden.
     */
    public void printTournament() {
        // Falls kein Event ausgewählt Fehler ausgeben
        if (currentEvent == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Turnier als PDF exportieren");
            alert.setHeaderText(null);
            alert.setContentText("Kein Event ausgewählt!");
            alert.showAndWait();
            return;
        }
        if (currentEvent == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Turnier löschen");
            alert.setHeaderText(null);
            alert.setContentText("Kein Event ausgewählt!");
            alert.showAndWait();
            return;
        }

        List<String> tournamentNameList = new ArrayList<String>();

        for (int i = 0; i < currentEvent.getTournamentList().size(); i++) {
            tournamentNameList.add(currentEvent.getTournamentList().get(i).getName());
        }
        if (tournamentNameList.isEmpty()) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText("Kein Turnier vorhanden!");
            alert.setContentText("In dieser Verantaltung existieren keine Turniere die exportiert werden können!");
            alert.showAndWait();
            return;
        }

        ChoiceDialog<String> dialog;
        dialog = new ChoiceDialog<>(currentEvent.getTournamentList().get(0).getName(), tournamentNameList);
        dialog.setTitle("PDF-Export");
        dialog.setHeaderText("Einzelnes Turnier als PDF exportieren");
        dialog.setContentText("Wählen Sie ein Turnier:");
        // Buttons umbenennen/anpassen
        Button cancelButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancelButton.setText("Abbrechen");
        Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        okButton.setText("Exportieren");

        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()) {
            // Dialog zum Aussuchen des Speicherorts und Dateinamens
            FileChooser fileChooser = new FileChooser();
            // Standardverzeichnis auswählen
            fileChooser.setInitialDirectory(
                    new File(System.getProperty("user.home")));
            fileChooser.setTitle("Wählen Sie einen Speicherort");
            // Nur Dateien mit der Endung ".pdf" zeigen
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Turniermaster Turnier (.pdf)", "*.pdf")
            );
            File file = fileChooser.showSaveDialog((Stage) root.getScene().getWindow());
            if (file != null) {
                PdfPrinter printer = new PdfPrinter();
                printer.printTournament(file, currentEvent, currentEvent.getTournamentList().get(tournamentNameList.indexOf(result.get())));

                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText("Turnier als PDF exportieren.");
                alert.setContentText("PDF-Export erfolgreich durchgeführt!");
                alert.showAndWait();
            }
        }
    }

    /**
     * Diese Methode bearbeitet ein Vorhandenes Turnier.
     */

    public void editTournament() {
        // Falls kein Event ausgewählt Fehler ausgeben
        if (currentEvent == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Turnier bearbeiten");
            alert.setHeaderText(null);
            alert.setContentText("Kein Event ausgewählt!");
            alert.showAndWait();
            return;
        }
        // Variablen die fürs Bearbeiten und Auswählen des Turniers gebraucht werden
        List tournamentNameList = new ArrayList<String>();
        int editTournamentAtIndex = -1;
        int sizeOfTournamentList = currentEvent.getTournamentList().size();
        // Füllen der ArrayList mit Namen der Turniere des ausgewählten Events
        for (int i = 0; i < currentEvent.getTournamentList().size(); i++) {
            tournamentNameList.add(currentEvent.getTournamentList().get(i).getName());
        }

        // Abfragen ob es überhaupt Turniere gibt
        if (sizeOfTournamentList == 0) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText(null);
            alert.setContentText("Event hat noch keine Turniere die bearbeitet werden können! Bitte zuerst ein Turnier erstellen!");

            alert.showAndWait();
            return;
        }

        // Erstellen des Dialogs
        ChoiceDialog<String> dialog;
        dialog = new ChoiceDialog<>(currentEvent.getTournamentList().get(0).getName(), tournamentNameList);
        dialog.setTitle("Turnier bearbeiten");
        dialog.setHeaderText(null);
        dialog.setContentText("Wählen Sie ein Turnier, dass sie bearbeiten wollen:");

        // Buttons umbenennen/anpassen
        Button cancelButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancelButton.setText("Abbrechen");
        Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        okButton.setText("Bearbeiten");

        // Dialog Zeichnen
        Optional<String> result = dialog.showAndWait();

        // Wenn in der ChoiceBox etwas ausgewählt
        if (result.isPresent()) {
            // Durchlaufe Array maximal so lange wie es Turniere gibt
            for (int i = 0; i < sizeOfTournamentList; i++) {
                // Erhöhe bei jedem durchlauf Löschvariable um 1
                editTournamentAtIndex++;
                // Falls das gesuchte Turnier das Ausgewählte ist
                if (currentEvent.getTournamentList().get(i).getName().matches(result.get())) {
                    // Eigentliche Turnierbearbeitung
                    // Eigenschaften vom Spieler aufrufen

                    String editTournamentName = currentEvent.getTournamentList().get(editTournamentAtIndex).getName();
                    String editTournamentDate = currentEvent.getTournamentList().get(editTournamentAtIndex).getDate();
                    String editTournamentTime = currentEvent.getTournamentList().get(editTournamentAtIndex).getTime();

                    // Uhrzeit in HH:mm splitten
                    String[] splitTime = editTournamentTime.split(":");

                    // Dialog mit Buttons erstellen
                    Dialog dialog2 = new Dialog<>();
                    dialog2.setTitle("Turnier Bearbeiten");
                    dialog2.setHeaderText("Daten von " + "'" + editTournamentName + "'" + " bearbeiten.");
                    ButtonType confirmEdit = new ButtonType("Änderung Speichern");
                    ButtonType cancelEdit = new ButtonType("Abbrechen", ButtonBar.ButtonData.CANCEL_CLOSE);
                    dialog2.getDialogPane().getButtonTypes().addAll(confirmEdit, cancelEdit);

                    // Erstellen der GridPane
                    GridPane grid = new GridPane();
                    grid.setHgap(10);
                    grid.setVgap(10);
                    grid.setPadding(new Insets(50, 150, 10, 10));

                    // Erstellen von Elementen
                    TextField editTournamentNameField = new TextField();
                    editTournamentNameField.setPromptText(editTournamentName);
                    editTournamentNameField.setText(editTournamentName);
                    DatePicker editTournamentDatePicker = new DatePicker();
                    editTournamentDatePicker.setPromptText(editTournamentDate);
                    editTournamentDatePicker.setEditable(false);
                    ChoiceBox editTournamentHours = new ChoiceBox();
                    editTournamentHours.setItems(hours);
                    editTournamentHours.setValue(splitTime[0]);
                    ChoiceBox editTournamentMinutes = new ChoiceBox();
                    editTournamentMinutes.setItems(minutes);
                    editTournamentMinutes.setValue(splitTime[1]);

                    // Platzieren der Element auf Pane
                    grid.add(new Label("Name:"), 0, 0);
                    grid.add(editTournamentNameField, 1, 0);
                    grid.add(new Label("Datum:"), 0, 1);
                    grid.add(editTournamentDatePicker, 1, 1);
                    grid.add(new Label("Uhrzeit (HH):"), 0, 2);
                    grid.add(editTournamentHours, 1, 2);
                    grid.add(new Label("Uhrzeit (mm):"), 0, 3);
                    grid.add(editTournamentMinutes, 1, 3);

                    // Hinzufügen der Elemente auf GridPane und Anzeigen vom Dialog
                    dialog2.getDialogPane().setContent(grid);
                    Optional<ButtonType> result2 = dialog2.showAndWait();

                    // Wenn Änderung bestätigt wird
                    if (result2.get() == confirmEdit) {
                        if (editTournamentDatePicker.getValue() != null && editTournamentNameField.getText() != "") {
                            // Werte ändern
                            currentEvent.getTournamentList().get(editTournamentAtIndex).setName(editTournamentNameField.getText());
                            currentEvent.getTournamentList().get(editTournamentAtIndex).setDate(editTournamentDatePicker.getValue().toString());
                            currentEvent.getTournamentList().get(editTournamentAtIndex).setTime((String) editTournamentHours.getValue() + ":" + (String) editTournamentMinutes.getValue());

                            // Dialog schließen und Event resetten
                            selectEvent();
                            dialog.close();
                        } else {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Fehler");
                            alert.setHeaderText("Ungültige Eingabe!");
                            alert.setContentText("Leere Felder bitte ausfüllen! Änderung Fehlgeschlagen!");
                            alert.showAndWait();
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Diese Methode gibt nur Informationen über das Projekt aus
     */
    public void showAbout(){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Über...");
        alert.setHeaderText("Turniermaster");
        alert.setContentText("Dieses Programm wurde beim Softwarepraktikum im Wintersemester 2014/15 von der Gruppe 24 geschrieben.");

        alert.showAndWait();
    }
    
    /**
     * Wähle AdminPassword aus
     */
    public void exportPasswordList(){
        if(!LoginWindowController.adminPasswords.isEmpty()){
            AdminPasswords passwords = new AdminPasswords(LoginWindowController.adminPasswords);
            
            // Dialog zum Aussuchen des Speicherorts und Dateinamens
            FileChooser fileChooser = new FileChooser();
            // Standardverzeichnis auswählen
            fileChooser.setInitialDirectory(
                    new File(System.getProperty("user.home")));
            fileChooser.setTitle("Wählen Sie einen Speicherort");
            fileChooser.setInitialFileName("Turniermaster Passwörter");
            // Nur Dateien mit der Endung ".psw" zeigen
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Passwortliste (.psw)", "*.psw")
            ); 
            File file = fileChooser.showSaveDialog((Stage) root.getScene().getWindow());
                if (file != null) {
                    FileUtil.exportToFile(passwords, file);
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText(null);
                    alert.setContentText("Passwortliste erfolgreich exportieret!");

                    alert.showAndWait();
                }
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText(null);
            alert.setContentText("Es befinden sich keine Passwörter in der Liste!");

            alert.showAndWait();
        }
    }
    
    /** 
     * Diese Methode fügt Admin-Passwörter in die Passwort-Liste hinzu
     */
    public void addAdminPasswords(){
        //Stringdialog
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Admin-Passwort hinzufügen");
        dialog.setHeaderText(null);
        dialog.setContentText("Passwort Eingeben:");
        Button okButton3 = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        okButton3.setText("Zur Passwortliste hinzufügen");
        Button cancelButton4 = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancelButton4.setText("Abbrechen");
            
        Optional<String> result = dialog.showAndWait();
            // Wenn String nicht leer
            if (result.isPresent() && !result.get().matches("")){
                // Wenn Liste nicht leer
                if(!LoginWindowController.adminPasswords.isEmpty()){
                    // Schaue ob es das Passwort nicht schon gibt
                    for(int i = 0; i < LoginWindowController.adminPasswords.size(); i++){
                        // Falls es das Passwort gibt starte Methode neu und verbiete das hinzfügen
                       if(LoginWindowController.adminPasswords.get(i).matches(result.get())){
                           Alert alert = new Alert(AlertType.ERROR);
                           alert.setTitle("Fehler");
                           alert.setHeaderText(result.get());
                           alert.setContentText("Dieses Passwort ist schon in der Passwortliste! Fügen sie ein neues Passwort hinzu.");

                           alert.showAndWait();
                           addAdminPasswords();
                       } 
                    }
                } 
                
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Hinzufügen bestätigen");
                alert.setHeaderText(result.get());
                alert.setContentText("Wollen sie dieses Passwort wirklich zur Passwortliste hinzufügen?");
                Button okButton1 = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
                okButton1.setText("Ja");
                Button cancelButton1 = (Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL);
                cancelButton1.setText("Abbrechen");
                
                Optional<ButtonType> resultButton = alert.showAndWait();
                if (resultButton.get() == ButtonType.OK){
                    LoginWindowController.adminPasswords.add(result.get()); 
                } else {
                    return;
                }             
            } else if (result.isPresent() && result.get().matches("")) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Fehler");
                alert.setHeaderText(result.get());
                alert.setContentText("Sie müssen ein Passwort eingeben!");

                alert.showAndWait();
                addAdminPasswords();
            } else {
                return;
            }     
    }
    /**
     * Entfernt ein Passwort aus der Passwortliste
     */
    public void removePasswords(){
        
        if(LoginWindowController.adminPasswords.isEmpty()){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText(null);
            alert.setContentText("Es existieren keine Passwörter in der Passwortliste!");

            alert.showAndWait();
            return;
        }
        // Dialog mit allen Passwörtern in der Passwortliste
        ChoiceDialog<String> dialog;
        dialog = new ChoiceDialog<>(LoginWindowController.adminPasswords.get(0), LoginWindowController.adminPasswords);
        dialog.setTitle("Passwort entfernen");
        dialog.setHeaderText("Passwort aus der Passwortliste entfernen");
        dialog.setContentText("Wählen Sie ein Passwort:");
        // Buttons umbenennen/anpassen
        Button cancelButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancelButton.setText("Abbrechen");
        Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        okButton.setText("Entfernen");

        Optional<String> result = dialog.showAndWait();
        // Falls Passwort ausgewählt
        if (result.isPresent()) {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Hinzufügen bestätigen");
            alert.setHeaderText(result.get());
            alert.setContentText("Wollen sie dieses Passwort wirklich aus der Passwortliste entfernen?");
            Button okButton1 = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
            okButton1.setText("Ja");
            Button cancelButton1 = (Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL);
            cancelButton1.setText("Abbrechen");
                
            Optional<ButtonType> resultButton = alert.showAndWait();
            // Falls bestätigt
            if (resultButton.get() == ButtonType.OK){
                // Entferne Passwort
                    LoginWindowController.adminPasswords.remove(result.get());
                } else {
                    return;
                }      
            
        } 
    
    }
    
}
