/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class KOTree implements Serializable {

    private TreeNode head;
    private List<TreeNode> nodes;
    private Tournament tournament;

    public KOTree(int playersInKO, Tournament tournament) {
        nodes = new ArrayList<TreeNode>();
        if (!tournament.isDoubleKO()) {
            for (int i = 0; i < 15; i++) {
                TreeNode node = new TreeNode(new Match(new Player("TBD", "", "", "", "", ""), new Player("TBD", "", "", "", "", ""), 0, tournament));
                nodes.add(node);
            }
            nodes.get(0).setNextWinner(nodes.get(8));
            nodes.get(1).setNextWinner(nodes.get(8));
            nodes.get(2).setNextWinner(nodes.get(9));
            nodes.get(3).setNextWinner(nodes.get(9));
            nodes.get(4).setNextWinner(nodes.get(10));
            nodes.get(5).setNextWinner(nodes.get(10));
            nodes.get(6).setNextWinner(nodes.get(11));
            nodes.get(7).setNextWinner(nodes.get(11));
            nodes.get(8).setNextWinner(nodes.get(12));
            nodes.get(9).setNextWinner(nodes.get(12));
            nodes.get(10).setNextWinner(nodes.get(13));
            nodes.get(11).setNextWinner(nodes.get(13));
            nodes.get(12).setNextWinner(nodes.get(14));
            nodes.get(13).setNextWinner(nodes.get(14));
        } else {
            for (int i = 0; i < 30; i++) {
                TreeNode node = new TreeNode(new Match(new Player("TBD", "", "", "", "", ""), new Player("TBD", "", "", "", "", ""), 0, tournament));
                nodes.add(node);
            }
            nodes.get(0).setNextWinner(nodes.get(12));
            nodes.get(1).setNextWinner(nodes.get(12));
            nodes.get(2).setNextWinner(nodes.get(13));
            nodes.get(3).setNextWinner(nodes.get(13));
            nodes.get(4).setNextWinner(nodes.get(14));
            nodes.get(5).setNextWinner(nodes.get(14));
            nodes.get(6).setNextWinner(nodes.get(15));
            nodes.get(7).setNextWinner(nodes.get(15));
            nodes.get(8).setNextWinner(nodes.get(16));
            nodes.get(9).setNextWinner(nodes.get(17));
            nodes.get(10).setNextWinner(nodes.get(18));
            nodes.get(11).setNextWinner(nodes.get(19));
            nodes.get(12).setNextWinner(nodes.get(20));
            nodes.get(13).setNextWinner(nodes.get(20));
            nodes.get(14).setNextWinner(nodes.get(21));
            nodes.get(15).setNextWinner(nodes.get(21));
            nodes.get(16).setNextWinner(nodes.get(22));
            nodes.get(17).setNextWinner(nodes.get(22));
            nodes.get(18).setNextWinner(nodes.get(23));
            nodes.get(19).setNextWinner(nodes.get(23));
            nodes.get(20).setNextWinner(nodes.get(26));
            nodes.get(21).setNextWinner(nodes.get(26));
            nodes.get(22).setNextWinner(nodes.get(24));
            nodes.get(23).setNextWinner(nodes.get(25));
            nodes.get(24).setNextWinner(nodes.get(27));
            nodes.get(25).setNextWinner(nodes.get(27));
            nodes.get(26).setNextWinner(nodes.get(29));
            nodes.get(27).setNextWinner(nodes.get(28));
            nodes.get(28).setNextWinner(nodes.get(29));
            nodes.get(0).setNextLoser(nodes.get(8));
            nodes.get(1).setNextLoser(nodes.get(8));
            nodes.get(2).setNextLoser(nodes.get(9));
            nodes.get(3).setNextLoser(nodes.get(9));
            nodes.get(4).setNextLoser(nodes.get(10));
            nodes.get(5).setNextLoser(nodes.get(10));
            nodes.get(6).setNextLoser(nodes.get(11));
            nodes.get(7).setNextLoser(nodes.get(11));
            nodes.get(12).setNextLoser(nodes.get(19));
            nodes.get(13).setNextLoser(nodes.get(18));
            nodes.get(14).setNextLoser(nodes.get(17));
            nodes.get(15).setNextLoser(nodes.get(16));
            nodes.get(20).setNextLoser(nodes.get(25));
            nodes.get(21).setNextLoser(nodes.get(24));
            nodes.get(26).setNextLoser(nodes.get(28)); 
            if (tournament.getPlayersInKO()==4){
                nodes.get(20).setNextLoser(nodes.get(27));
                nodes.get(21).setNextLoser(nodes.get(27));
            } else if (tournament.getPlayersInKO()==8){
                nodes.get(12).setNextLoser(nodes.get(22));
                nodes.get(13).setNextLoser(nodes.get(22));
                nodes.get(14).setNextLoser(nodes.get(23));
                nodes.get(15).setNextLoser(nodes.get(23));
            }
        }
        
        this.tournament = tournament;
    }
    
    

    

    public void initialize(List<Player> KOPlayers) {
        if (KOPlayers.size() == 16) {
            nodes.get(0).setMatch(new Match(KOPlayers.get(0), KOPlayers.get(15), 0, tournament));
            nodes.get(1).setMatch(new Match(KOPlayers.get(1), KOPlayers.get(14), 0, tournament));
            nodes.get(2).setMatch(new Match(KOPlayers.get(2), KOPlayers.get(13), 0, tournament));
            nodes.get(3).setMatch(new Match(KOPlayers.get(3), KOPlayers.get(12), 0, tournament));
            nodes.get(4).setMatch(new Match(KOPlayers.get(4), KOPlayers.get(11), 0, tournament));
            nodes.get(5).setMatch(new Match(KOPlayers.get(5), KOPlayers.get(10), 0, tournament));
            nodes.get(6).setMatch(new Match(KOPlayers.get(6), KOPlayers.get(9), 0, tournament));
            nodes.get(7).setMatch(new Match(KOPlayers.get(7), KOPlayers.get(8), 0, tournament));
        } else if (KOPlayers.size() == 8) {
            if (!tournament.isDoubleKO()){
            nodes.get(8).setMatch(new Match(KOPlayers.get(0), KOPlayers.get(7), 0, tournament));
            nodes.get(9).setMatch(new Match(KOPlayers.get(1), KOPlayers.get(6), 0, tournament));
            nodes.get(10).setMatch(new Match(KOPlayers.get(2), KOPlayers.get(3), 0, tournament));
            nodes.get(11).setMatch(new Match(KOPlayers.get(4), KOPlayers.get(5), 0, tournament));
            } else {
            nodes.get(12).setMatch(new Match(KOPlayers.get(0), KOPlayers.get(7), 0, tournament));
            nodes.get(13).setMatch(new Match(KOPlayers.get(1), KOPlayers.get(6), 0, tournament));
            nodes.get(14).setMatch(new Match(KOPlayers.get(2), KOPlayers.get(5), 0, tournament));
            nodes.get(15).setMatch(new Match(KOPlayers.get(3), KOPlayers.get(4), 0, tournament));
            }
        } else if (KOPlayers.size() == 4) {
            if (!tournament.isDoubleKO()){
            nodes.get(12).setMatch(new Match(KOPlayers.get(0), KOPlayers.get(3), 0, tournament));
            nodes.get(13).setMatch(new Match(KOPlayers.get(1), KOPlayers.get(2), 0, tournament));
            } else {
            nodes.get(20).setMatch(new Match(KOPlayers.get(0), KOPlayers.get(3), 0, tournament));
            nodes.get(21).setMatch(new Match(KOPlayers.get(1), KOPlayers.get(2), 0, tournament)); 
            }
        } else {
            System.err.println("Ungültige Anzahl von KO-Spielern!");
        }
    }

    public TreeNode getHead() {
        return head;
    }

    public void setHead(TreeNode head) {
        this.head = head;
    }

    public void enterResult(int matchNumber, Player winner) {
        if (matchNumber % 2 == 1) {
            nodes.get(matchNumber - 1).getNextWinner().getMatch().setPlayer1(winner);
        } else {
            nodes.get(matchNumber - 1).getNextWinner().getMatch().setPlayer2(winner);
        }

    }

    public void enterScore(int index, int score2, int score1) {
        Player winner;
        Player loser;
        if (!nodes.get(index).isFinished()) {
            if (score1 > score2) {
                winner = nodes.get(index).getMatch().getPlayer1();
                loser = nodes.get(index).getMatch().getPlayer2();
            } else {
                winner = nodes.get(index).getMatch().getPlayer2();
                loser = nodes.get(index).getMatch().getPlayer1();
            }

            nodes.get(index).getMatch().setScore1(tournament, score1);
            nodes.get(index).getMatch().setScore2(tournament, score2);
            
            if (nodes.get(index).getNextLoser() != null){
                if (nodes.get(index).getNextLoser().getMatch().getPlayer1().getFirstName().equals("TBD")){
                    nodes.get(index).getNextLoser().getMatch().setPlayer1(loser);
                } else {
                    nodes.get(index).getNextLoser().getMatch().setPlayer2(loser);
                }
            }
            
            if (index % 2 == 1) {
                if (nodes.get(index).getNextWinner() != null) {
                    if (nodes.get(index).getNextWinner().getMatch().getPlayer1().getFirstName().equals("TBD")){
                      nodes.get(index).getNextWinner().getMatch().setPlayer1(winner);
                    } else {
                      nodes.get(index).getNextWinner().getMatch().setPlayer2(winner);  
                    }
                }
            } else {
                if (nodes.get(index).getNextWinner() != null) {
                    if (nodes.get(index).getNextWinner().getMatch().getPlayer2().getFirstName().equals("TBD")){
                       nodes.get(index).getNextWinner().getMatch().setPlayer2(winner);
                    } else {
                       nodes.get(index).getNextWinner().getMatch().setPlayer1(winner);  
                    }
                }
            }
        }

        nodes.get(index).setFinished(true);
    }

    public Match getMatch(int index) {
        return nodes.get(index).getMatch();
    }

    public Tournament getTournament() {
        return tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    public void disqualify(Player player) {
        for (int i=0;i<nodes.size();i++){
            if (nodes.get(i).getMatch().getPlayer1().equals(player)){
                nodes.get(i).getMatch().setScore2(tournament, tournament.getFreePassValue());
                nodes.get(i).getMatch().setScore1(tournament, 0);
                nodes.get(i).getMatch().setFinished(true);
            } else if (nodes.get(i).getMatch().getPlayer2().equals(player)) {
                nodes.get(i).getMatch().setScore1(tournament, tournament.getFreePassValue());
                nodes.get(i).getMatch().setScore2(tournament, 0);
                nodes.get(i).getMatch().setFinished(true);
            }
        }
    }

}
