package turniermaster.gui;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import turniermaster.data.Match;
import turniermaster.data.Player;
import turniermaster.data.Round;
import turniermaster.data.Tournament;

/**
 * Diese Klasse kontrolliert das Fenster zur Darstellung der Partien einer Runde
 * eines Turniers.
 *
 * @author admin
 */
public class RoundViewController implements Initializable {

    private int roundNumber;
    private Tournament tournament;
    private List<Match> matches = new ArrayList<Match>();
    private TournamentViewController parentController;
    private Round round;

    @FXML
    private TableColumn playerName1;
    @FXML
    private TableColumn playerName2;
    @FXML
    private TableColumn score1;
    @FXML
    private TableColumn score2;
    @FXML
    private TableView matchTable;
    @FXML
    private TableColumn tableNumber;
    @FXML
    private Button buttonNextRound;
    @FXML
    private Button buttonSwapPlayers;
    @FXML
    private TableColumn winPointsColumn1;
    @FXML
    private TableColumn winPointsColumn2;
    

    /**
     * Initialisiert den Controller
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        matchTable.setOnMousePressed((e) -> {
            if (e.getClickCount() == 2) {
                enterResult();
            }

        });

    }

    public int getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }

    public void setMatches(ArrayList<Match> matches) {
        //this.matches=matches;
    }

    public Tournament getTournament() {
        return tournament;
    }

    /**
     * Übergibt das Turnier, zu dem diese Runde gehört
     *
     * @param tournament Turnier
     * @param controller Controller des Turniers
     */
    public void setTournament(Tournament tournament, TournamentViewController controller) {

        this.parentController = controller;

        this.tournament = tournament;
        

        // Füllt die Spalten mit den Namen aller Spieler aus den Partien diser Runde
        playerName1.setCellValueFactory(new Callback<CellDataFeatures<Match, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Match, String> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getPlayer1().getFirstName() + " " + p.getValue().getPlayer1().getLastName());
            }
        });

        playerName2.setCellValueFactory(new Callback<CellDataFeatures<Match, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Match, String> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getPlayer2().getFirstName() + " " + p.getValue().getPlayer2().getLastName());
            }
        });
        
        

        // Füllt die Spalten mit den Ergebnissen der Partien dieser Runde.
        score1.setCellValueFactory(new PropertyValueFactory<>("score1"));
        score2.setCellValueFactory(new PropertyValueFactory<>("score2"));
        winPointsColumn1.setCellValueFactory(new PropertyValueFactory<>("winPoints1"));
        winPointsColumn2.setCellValueFactory(new PropertyValueFactory<>("winPoints2"));
        tableNumber.setCellValueFactory(new PropertyValueFactory<>("tableNumber"));
        this.matches = tournament.getRoundList().get(roundNumber - 1).getMatchList();
        if (!tournament.isUsesWinPoints()){
            winPointsColumn1.setVisible(false);
            winPointsColumn2.setVisible(false);
        }
        round = tournament.getRoundList().get(roundNumber - 1);    
        if (round.isFinished()){
            buttonNextRound.setDisable(true);
            buttonSwapPlayers.setDisable(true);
        }
        matchTable.getItems().setAll(matches);
    }

    /**
     * Erneuert die Darstellung der Tabelle (z.B. nach Änderungen an den
     * Tabellen)
     */
    public void refresh() {
        matchTable.getItems().setAll(matches);
    }

    /**
     * Öffnet einen Dialog zum Eintragen des Ergebnisses einer Partie
     */
    public void enterResult() {
        if (matchTable.getSelectionModel().getSelectedItem() != null) {
            Match match = (Match) matchTable.getSelectionModel().getSelectedItem();
            Optional<String> result12 = null;
            Optional<String> result22 = null;

            if (match.isFinished() == true) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Fehler");
                alert.setHeaderText("Ergebnis Eintragen");
                alert.setContentText("Dieses Match ist bereits vorbei!");
                alert.showAndWait();
                return;
            }

            if (match.getPlayer1().getFirstName().equals("FREILOS") || match.getPlayer2().getFirstName().equals("FREILOS")
                    ||match.getPlayer1().getFirstName().equals("SUPERFREILOS") || match.getPlayer2().getFirstName().equals("SUPERFREILOS")) {
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Bestätigung");
                alert.setHeaderText("Freilos");
                alert.setContentText("Wollen sie dieses Freilos-Spiel eintragen?");

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    match.setFinished(true);
                    match.setScore1(tournament, tournament.getFreePassValue());
                    refresh();
                    parentController.refresh();
                    return;
                } else {
                    return;
                }
            }

            TextInputDialog dialog = new TextInputDialog("0");
            dialog.setTitle("Ergebnis eintragen");
            dialog.setHeaderText("Ergebnis eintragen");
            dialog.setContentText(match.getPlayer1().getFirstName() + " " + match.getPlayer1().getLastName() + ":");
            Button okButton1 = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
            okButton1.setText("Punkte eintragen");
            Button cancelButton1 = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
            cancelButton1.setText("Abbrechen");

            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                if (result.get().matches("-?[0-9]+")) {
                    //   match.setScore1(Integer.parseInt(result.get()));
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Ungültige Eingabe!");
                    alert.setContentText("Eintragen der Ergebnise fehlgeschlagen!");
                    alert.showAndWait();
                    return;
                }
            } else {
                return;
            }

            if (tournament.isUsesWinPoints()) {
                TextInputDialog dialog12 = new TextInputDialog("0");
                dialog12.setTitle("Siegpunkte eintragen");
                dialog12.setHeaderText("Höhe des Sieges eintragen");
                dialog12.setContentText(match.getPlayer1().getFirstName() + " " + match.getPlayer1().getLastName() + ":");
                Button okButton12 = (Button) dialog12.getDialogPane().lookupButton(ButtonType.OK);
                okButton12.setText("Siegpunkte eintragen");
                Button cancelButton12 = (Button) dialog12.getDialogPane().lookupButton(ButtonType.CANCEL);
                cancelButton12.setText("Abbrechen");

                result12 = dialog12.showAndWait();
                if (result12.isPresent()) {
                    if (result12.get().matches("-?[0-9]+")) {
                    } else {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Fehler");
                        alert.setHeaderText("Ungültige Eingabe!");
                        alert.setContentText("Eintragen der Ergebnise fehlgeschlagen!");
                        alert.showAndWait();
                        return;
                    }
                } else {
                    return;
                }
            }

            TextInputDialog dialog2 = new TextInputDialog("0");
            dialog2.setTitle("Ergebnis eintragen");
            dialog2.setHeaderText("Ergebnis eintragen");
            dialog2.setContentText(match.getPlayer2().getFirstName() + " " + match.getPlayer2().getLastName() + ":");
            Button okButton2 = (Button) dialog2.getDialogPane().lookupButton(ButtonType.OK);
            okButton2.setText("Punkte eintragen");
            Button cancelButton2 = (Button) dialog2.getDialogPane().lookupButton(ButtonType.CANCEL);
            cancelButton2.setText("Abbrechen");
            Optional<String> result2 = dialog2.showAndWait();

            if (tournament.isUsesWinPoints()) {
                TextInputDialog dialog22 = new TextInputDialog("0");
                dialog22.setTitle("Siegpunkte eintragen");
                dialog22.setHeaderText("Höhe des Sieges eintragen");
                dialog22.setContentText(match.getPlayer2().getFirstName() + " " + match.getPlayer2().getLastName() + ":");
                Button okButton22 = (Button) dialog22.getDialogPane().lookupButton(ButtonType.OK);
                okButton22.setText("Siegpunkte eintragen");
                Button cancelButton22 = (Button) dialog22.getDialogPane().lookupButton(ButtonType.CANCEL);
                cancelButton22.setText("Abbrechen");

                result22 = dialog22.showAndWait();
                if (result22.isPresent()) {
                    if (result22.get().matches("-?[0-9]+")) {
                    } else {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Fehler");
                        alert.setHeaderText("Ungültige Eingabe!");
                        alert.setContentText("Eintragen der Ergebnise fehlgeschlagen!");
                        alert.showAndWait();
                        return;
                    }
                } else {
                    return;
                }
            }

            if (result2.isPresent()) {
                if (result2.get().matches("-?[0-9]+")) {
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Bestätigung - Punkte eintragen");
                    alert.setHeaderText(match.getPlayer1().getFirstName() + " " + match.getPlayer1().getLastName() + "       " + result.get() + "  :  " + result2.get() + "      " + match.getPlayer2().getFirstName() + " " + match.getPlayer2().getLastName());
                    alert.setContentText("Das Ergebnis kann nach Ihrer Bestätigung nicht mehr geändert werden! Fortfahren?");
                    Button okButton3 = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
                    okButton3.setText("Ja");
                    Button cancelButton3 = (Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL);
                    cancelButton3.setText("Abbrechen");

                    Optional<ButtonType> buttonResult = alert.showAndWait();

                    if (buttonResult.get() == ButtonType.OK) {
                        match.setScore1(tournament, Integer.parseInt(result.get()));
                        match.setScore2(tournament, Integer.parseInt(result2.get()));
                        if (tournament.isUsesWinPoints()){
                            match.setWinPoints1(tournament, Integer.parseInt(result12.get()));
                            match.setWinPoints2(tournament, Integer.parseInt(result22.get()));
                        }
                        match.setFinished(true);
                    } else {
                        return;
                    }

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Ungültige Eingabe!");
                    alert.setContentText("Eintragen der Ergebnise fehlgeschlagen!");
                    alert.showAndWait();
                    return;
                }
            } else {
                return;
            }

            if (roundNumber > tournament.getRounds()) {
                if (match.getScore1() != match.getScore2()) {

                    if (match.getScore1() > match.getScore2()) {
                        if (match.getPlayer2().isLostInKO(tournament)) {
                            match.getPlayer2().setLostDouble(tournament, true);
                        }
                        match.getPlayer2().setLostInKO(tournament, true);
                        match.setFinished(true);
                    } else {
                        if (match.getPlayer1().isLostInKO(tournament)) {
                            match.getPlayer1().setLostDouble(tournament, true);
                        }
                        match.getPlayer1().setLostInKO(tournament, true);
                        match.setFinished(true);
                    }
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Ungültige Eingabe!");
                    alert.setContentText("Unentschieden ist in einer KO-Runde nicht möglich! Eintragen fehlgeschlagen!");
                    alert.showAndWait();
                    match.setFinished(false);
                    match.setScore1(tournament, Integer.parseInt(result.get()));
                    match.setScore2(tournament, Integer.parseInt(result.get()));
                    return;
                }
            }
            // Mit dem Eintragen des Ergebnisses ist die Partie beendet.
            //    match.setFinished(true);
            parentController.refresh();

        }

        refresh();

        // Startet die Generierung der nächsten Runde wenn alle Partien beendet sind.
 /*       for (int i = 0; i < matches.size(); i++) {
         if (matches.get(i).isFinished() == false) {
         return;
         }
         }

         if (roundNumber == tournament.getRounds() + (int) Math.sqrt(tournament.getPlayersInKO())) {
         return;
         }
         parentController.generateMatches(roundNumber + 1); */
    }

    public void swapPlayers() {
        List playerNameList = new ArrayList<Player>();
        Player player1 = null;
        Player player2 = null;
        Player player3 = null;
        Player player4 = null;
        for (int i = 0; i < tournament.getPlayerList().size(); i++) {
            if(!tournament.getPlayerList().get(i).isDQ()){
                playerNameList.add(tournament.getPlayerList().get(i).getFirstName() + " "
                    + tournament.getPlayerList().get(i).getLastName());
            }
        }

        if (playerNameList.isEmpty()) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText("Spielerliste leer");
            alert.setContentText("Sie müssen zuerst Spieler hintufügen!");

            alert.showAndWait();
            return;
        }
        ChoiceDialog<String> dialog;
     //   dialog = new ChoiceDialog<>(tournament.getPlayerList().get(0).getFirstName() + " "
     //          + tournament.getPlayerList().get(0).getLastName(), playerNameList);
        dialog = new ChoiceDialog<>(playerNameList.get(0), playerNameList);
        dialog.setTitle("Spieler tauschen");
        dialog.setHeaderText("Spieler tauschen");
        dialog.setContentText("Wählen Sie den ersten Spieler:");
        Button cancelButton1 = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancelButton1.setText("Abbrechen");
        Button okButton1 = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        okButton1.setText("Spieler auswählen");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            player1 = tournament.getPlayerList().get(playerNameList.indexOf(result.get()));
        } else {
            return;
        }

     //   dialog = new ChoiceDialog<>(tournament.getPlayerList().get(0).getFirstName() + " "
     //          + tournament.getPlayerList().get(0).getLastName(), playerNameList);
        dialog = new ChoiceDialog<>(playerNameList.get(0), playerNameList);
        dialog.setTitle("Spieler tauschen");
        dialog.setHeaderText("Spieler tauschen");
        dialog.setContentText("Wählen Sie den zweiten Spieler:");
        Button cancelButton2 = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancelButton2.setText("Abbrechen");
        Button okButton2 = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        okButton2.setText("Spieler auswählen");
        result = dialog.showAndWait();
        if (result.isPresent()) {
            player2 = tournament.getPlayerList().get(playerNameList.indexOf(result.get()));
        } else {
            return;
        }

        if (player1.equals(player2)) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setHeaderText("Fehler beim Tauschen");
            alert.setContentText("Sie können einen Spieler nicht mit sich selbst tauschen!");
            alert.showAndWait();
        }

        for (int i = 0; i < matches.size(); i++) {
            if (matches.get(i).getPlayer1().equals(player1) || matches.get(i).getPlayer2().equals(player1)
                    || matches.get(i).getPlayer1().equals(player2) || matches.get(i).getPlayer2().equals(player2)) {
                if (matches.get(i).isFinished()) {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setHeaderText("Fehler beim Tauschen");
                    alert.setContentText("Sie können keine Spieler tauschen wenn ein Spiel bereits abgeschlossen wurde!");
                    alert.showAndWait();
                    return;
                }
            }
        }

        for (int i = 0; i < matches.size(); i++) {
            if ((matches.get(i).getPlayer1().equals(player1)
                    && matches.get(i).getPlayer2().equals(player2))
                    || (matches.get(i).getPlayer1().equals(player2)
                    && matches.get(i).getPlayer2().equals(player1))) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Fehler");
                alert.setHeaderText("Fehler beim Tauschen");
                alert.setContentText("Diese beiden Spieler spielen bereits gegeneinander!");
                alert.showAndWait();

            } else if (matches.get(i).getPlayer1().equals(player1)) {
                matches.get(i).setPlayer1(player2);
            } else if (matches.get(i).getPlayer2().equals(player1)) {
                matches.get(i).setPlayer2(player2);
            } else if (matches.get(i).getPlayer1().equals(player2)) {
                matches.get(i).setPlayer1(player1);
            } else if (matches.get(i).getPlayer2().equals(player2)) {
                matches.get(i).setPlayer2(player1);

            }

        }

        refresh();

    }

    /**
     * Diese Methode berechnet immer die nächste Runde eines Turniers (nicht die
     * erste)
     */
    public void nextRound() {
        // Startet die Generierung der nächsten Runde wenn alle Partien beendet sind.
        for (int i = 0; i < matches.size(); i++) {
            if (matches.get(i).isFinished() == false) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Fehler");
                alert.setHeaderText(null);
                alert.setContentText("Nicht alle Spiele wurden ausgewertet! Bitte alle Spiele auswerten!");
                alert.showAndWait();
                return;
            }
        }

        // if (roundNumber == tournament.getRounds() + (int) Math.sqrt(tournament.getPlayersInKO())) {
        //     return;
        // }
        round.setFinished(true);
        buttonNextRound.setDisable(true);
        buttonSwapPlayers.setDisable(true);
        parentController.generateMatches(roundNumber + 1);

    }

}
