/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.IO;

import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class FileUtilTest {

    private File file = new File("test.tst");
    private File fileImport = new File("testImport.tst");
    private String testString = "Hallo Welt!";

    public FileUtilTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
        file.delete();
        fileImport.delete();
    }

    /**
     * Test of exportToFile method, of class FileUtil.
     */
    @Test
    public void testExportToFile() throws Exception {
        System.out.println("exportToFile");
        FileUtil.exportToFile(testString, file);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(file.exists());
    }

    /**
     * Test of importFromFile method, of class FileUtil.
     */
    @Test
    public void testImportFromFile() throws Exception {
        FileUtil.exportToFile(testString, fileImport);
        System.out.println("importFromFile");
        Object expResult = "Hallo Welt!";
        Object result = FileUtil.importFromFile(fileImport);
        assertEquals(expResult, result);
    }

    @Test
    public void testImportException() {
        FileUtil.importFromFile(new File(System.getProperty("user.home") + "\\thisfolderdoesntexist\\test.tst"));
        /* Hier wird eigentlich nur getestet ob das Programm nicht abstürzt oder eine Exception wirft
         Außerdem kann man sehen ob die passende Meldung in der Konsole erscheint
         */
        assertTrue(true);
    }

    @Test
    public void testExportException() {
        FileUtil.exportToFile(testString, new File(System.getProperty("user.home") + "\\thisfolderdoesntexist\\test.tst"));
        /* Hier wird eigentlich nur getestet ob das Programm nicht abstürzt oder eine Exception wirft
         Außerdem kann man sehen ob die passende Meldung in der Konsole erscheint
         */
        assertTrue(true);
    }

}
