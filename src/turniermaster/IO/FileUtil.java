

package turniermaster.IO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Diese Klasse stellt Funktionen zum Speichern und Laden von Java-Objekten
 * als Binärdateien bereit. Sie wird zum Import und Export der Veranstaltungs-
 * und Turnierdaten benutzt.
 * 
 * @author admin
 */
public class FileUtil {
    
    /**
     * Speichert ein Java-Objekt als Binärdatei
     * @param o Java-Objekt das gespeichert werden soll
     * @param file Dateipfad und Dateiname
     */
    public static void exportToFile(Object o, File file){
        try {
        FileOutputStream output = new FileOutputStream(file);
        ObjectOutputStream objectOutput = new ObjectOutputStream(output);
        objectOutput.writeObject(o);
        objectOutput.close();
        output.close();
        } catch (FileNotFoundException e){
            System.err.println("Datei "+file.toString()+" nicht gefunden!");
        } catch (IOException e){
            System.err.println("Fehler beim Schreiben von "+file.toString()+"!");
        }
    }
    
    /**
     * Lädt ein Java-Objekt aus einer Binärdatei
     * @param file Dateipfad und Dateiname
     * @return Gibt ein Java-Objekt zurück
     */
    
    public static Object importFromFile(File file) {
        try{
        FileInputStream input = new FileInputStream(file);
        ObjectInputStream objectInput = new ObjectInputStream(input);
        Object o = objectInput.readObject();
        objectInput.close();
        input.close();
        return o;
        } catch (FileNotFoundException e){
            System.err.println("Datei "+file.toString()+" nicht gefunden!");
        } catch (IOException e) {
            System.err.println("Fehler beim Lesen von "+file.toString());
        } catch (ClassNotFoundException e){
            System.err.println("Datei "+file.toString()+" ist ungültig!");
        }
        return null;
    }
    
}
