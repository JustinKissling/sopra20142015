
package turniermaster.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasse die eine einzelne Runde darstellt.
 *
 * @author admin
 */
public class Round implements Serializable {
    String name;
    List<Match> matchList = new ArrayList<Match>();
    int players;
    boolean finished;
    
    public Round (String name) {
        this.name = name;
        this.finished = false;
    }

    public void addMatch(Match match) {
        matchList.add(match);
    }
    
    public int size(){
        return this.matchList.size();
    }
    
    public Match get(int index){
        return matchList.get(index);
    }
    
    public List<Match> getMatchList(){
        return this.matchList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Gibt alle Verlierer dieser Runde zurück
     * @return 
     */
    
    public ArrayList<Player> getLosers(){
        ArrayList<Player> returnList = new ArrayList<Player>();
        for (int i=0;i<matchList.size();i++){
            if (matchList.get(i).getScore1()>matchList.get(i).getScore2()){
                returnList.add(matchList.get(i).getPlayer2());
            } else {
                returnList.add(matchList.get(i).getPlayer1());
            }
        }
        
        return returnList;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
    
    
    
   
}
