/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.data;

import java.io.Serializable;

/**
 * Diese Klasse stellt eine Partie zwischen zwei Spielern dar. Es werden die
 * Spieler, der Punktestand, und ob das Spiel beendet ist gespeichert.
 *
 * @author admin
 */
public class Match implements Serializable {

    private int tableNumber;
    private Player player1;
    private Player player2;
    private int score1;
    private int score2;
    private boolean finished;
    private Tournament tournament;
    private int winPoints1;
    private int winPoints2;

    /**
     * Konstruktor
     *
     * @param player1 Spieler 1
     * @param player2 Spieler 2
     */
    public Match(Player player1, Player player2, int freePassValue, Tournament tournament) {
        this.player1 = player1;
        this.player2 = player2;
        this.score1 = 0;
        this.score2 = 0;
        this.winPoints1 = 0;
        this.winPoints2 = 0;
        this.finished = false;
        if (player1.isDQ()){
            this.score1 = 0;
            this.score2 = freePassValue;
            this.finished = true;
        } else if (player2.isDQ()){
            this.score2 = 0;
            this.score1 = freePassValue;
            this.finished = true; 
        }
        
    }

        public Match(Player player1, Player player2, int tableNumber, int freePassValue, Tournament tournament) {
        this.player1 = player1;
        this.player2 = player2;
        this.score1 = 0;
        this.score2 = 0;
        this.winPoints1 = 0;
        this.winPoints2 = 0;
        this.finished = false;
        this.tableNumber = tableNumber+1;
        if (player1.isDQ()){
            this.score1 = 0;
            this.score2 = freePassValue;
            this.finished = true;
        } else if (player2.isDQ()){
            this.score2 = 0;
            this.score1 = freePassValue;
            this.finished = true; 
        }
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public int getScore1() {
        return score1;
    }

    /**
     * Trägt die Punkte für Spieler 1 ein und schreibt sie seiner Tabellenstärke
     * gut
     *
     * @param score1 Punkte für Spieler 1
     */
    public void setScore1(Tournament tournament, int score1) {
        this.score1 = score1;
        player1.addOpponent(player2,tournament);
        player1.addTableStrength(score1, tournament);
    }

    public int getScore2() {
        return score2;
    }

    /**
     * Trägt die Punkte für Spieler 2 ein und schreibt sie seiner Tabellenstärke
     * gut
     *
     * @param score2 Punkte für Spieler 2
     */
    public void setScore2(Tournament tournament, int score2) {
        this.score2 = score2;
        player2.addOpponent(player1,tournament);
        player2.addTableStrength(score2, tournament);
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public int getWinPoints1() {
        return winPoints1;
    }

    public void setWinPoints1(Tournament tournament,int points1) {
        this.winPoints1 = points1;
        player1.addWinPoints(tournament, points1);
    }

    public int getWinPoints2() {
        return winPoints2;
    }
    
    public void setWinPoints2(Tournament tournament,int points2) {
        this.winPoints2 = points2;
        player2.addWinPoints(tournament, points2);
    }
    
    

}
