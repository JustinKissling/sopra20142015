
package turniermaster.gui;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import turniermaster.IO.FileUtil;
import turniermaster.data.AdminPasswords;

/**
 *Steurt das Login-Fenster
 * 
 * @author admin
 */
public class LoginWindowController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private TextField adminPasswordField;
            
    public static boolean isAdmin = false;
    public static ArrayList<String> adminPasswords = new ArrayList<String>();
    
/**
 * Event-Handler für den Button
 * TODO: Passwort überprüfen
 * @param event
 * @throws IOException 
 */
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Turniermaster");
        stage.setScene(new Scene(root, visualBounds.getWidth(), visualBounds.getHeight()));
        stage.setMaximized(true);
        stage.show();
        // aktuelles Fenster schließen
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }
    
    @FXML
    private void handleButtonAction2(ActionEvent event) throws IOException {
        if(!adminPasswords.isEmpty()){
            for(int i = 0; i < adminPasswords.size(); i++){
            if(adminPasswordField.getText().matches(adminPasswords.get(i)) || adminPasswordField.getText().matches("")){
               isAdmin = true;
            } 
          } 
        } else if(adminPasswordField.getText().matches("admin")){
            isAdmin = true;
        }
       if(isAdmin == true){
           Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
           Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
           Stage stage = new Stage();
           stage.setTitle("Turniermaster");
           stage.setScene(new Scene(root, visualBounds.getWidth(), visualBounds.getHeight()));
           stage.setMaximized(true);
           stage.show();
           // aktuelles Fenster schließen
           ((Node)(event.getSource())).getScene().getWindow().hide();  
           return;
       } else {
           adminPasswordField.setText("");
           Alert alert = new Alert(AlertType.ERROR);
           alert.setTitle("Fehler");
           alert.setHeaderText(null);
           alert.setContentText("Ungültiges Admin-Passwort!");

           alert.showAndWait();
       }
    } 
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Passwörter importieren");
        alert.setHeaderText(null);
        alert.setContentText("Wollen sie eine Passwortliste importieren um sich als Administrator anmelden zu können?");
        Button okButton1 = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
        okButton1.setText("Ja");
        Button cancelButton1 = (Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancelButton1.setText("Nein");
        
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
            );
            fileChooser.setTitle("Wählen Sie eine Datei");
            fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Turniermaster Passwortliste (.psw)", "*.psw")
            );
        File file = fileChooser.showOpenDialog(null);
            if (file != null){
             AdminPasswords password =  (AdminPasswords) FileUtil.importFromFile(file);
             adminPasswords = password.getPasswordList();
             
             Alert alert2 = new Alert(AlertType.INFORMATION);
             alert2.setTitle("Information");
             alert2.setHeaderText(null);
             alert2.setContentText("Passwortliste erfolgreich importiert!");

             alert2.showAndWait();
            }
        } else {
           return;
         }
    }    
    
}
