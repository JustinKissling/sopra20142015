/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turniermaster.data;

import java.io.Serializable;

/**
 *
 * @author admin
 */
public class TreeNode implements Serializable {
    private Match match;
    private TreeNode previous1;
    private TreeNode previous2;
    private TreeNode nextWinner;
    private TreeNode nextLoser;
    private boolean finished;
    
    public TreeNode(Match match){
        this.match = match;
        this.finished = false;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public TreeNode getPrevious1() {
        return previous1;
    }

    public void setPrevious1(TreeNode previous) {
        this.previous1 = previous;
    }
    
    public TreeNode getPrevious2() {
        return previous2;
    }

    public void setPrevious2(TreeNode previous) {
        this.previous2 = previous;
    }

    public TreeNode getNextWinner() {
        return nextWinner;
    }

    public void setNextWinner(TreeNode nextWinner) {
        this.nextWinner = nextWinner;
    }

    public TreeNode getNextLoser() {
        return nextLoser;
    }

    public void setNextLoser(TreeNode nextLoser) {
        this.nextLoser = nextLoser;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
    
    
    
}
